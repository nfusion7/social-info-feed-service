package util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

import play.Play;

public class ConnectionUtil {

	public static HttpURLConnection createConnection(String url) throws IOException {
		HttpURLConnection connection;
		URL connectionUrl = new URL(url);
		String proxyHost = Play.configuration.getProperty("http.proxyHost",System.getProperty("http.proxyHost"));
		String proxyPort = Play.configuration.getProperty("http.proxyPort",System.getProperty("http.proxyPort"));
		Proxy proxy;
		if (proxyHost == null || proxyPort == null)
			proxy = Proxy.NO_PROXY;
		else {
			proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost,
					Integer.valueOf(proxyPort)));
		}
		connection = (HttpURLConnection) connectionUrl.openConnection(proxy);
		connection.setConnectTimeout(10000);
		connection.setReadTimeout(10000);
		return connection;
	}
}
