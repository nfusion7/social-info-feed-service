/**
 * 
 */
package util;


import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Level;

import play.Logger;

public class FeedserviceLogger extends Logger {

	private static String lineSeparator = "`";
	private static List<String> urlParamsToSanitize = new LinkedList<String>();
	
	static{
		urlParamsToSanitize.add("OauthToken=");
		urlParamsToSanitize.add("OauthSecret=");
	}
	
	public static void log(Level logLevel, String requestUuid, String url,long requestTimeInMs,String message) {
		StringBuilder logLineSB = new StringBuilder();
		 
		String[] logLineItems = {logLevel.toString(), requestUuid, sanitizeUrl(url), String.valueOf(requestTimeInMs), message};

		for (String s : logLineItems) {
			logLineSB.append(s);
			logLineSB.append(lineSeparator);
		}
		String logLine = logLineSB.toString();
		
		if(logLevel.equals(Level.DEBUG) && isDebugEnabled()){
			debug(logLine);
		}else if(logLevel.equals(Level.INFO)){
			info(logLine);
		}else if(logLevel.equals(Level.WARN)){
			warn(logLine);
		}else if(logLevel.equals(Level.ERROR)){
			error(logLine);
		}
	}
	/**
     * Removes the values of sensitive url params like OauthToken, OauthSecret. 
     * To be used before logging.
     * @param url
     * @return
     */
	private static String sanitizeUrl(String url){
		if(url != null){
			for(String param : urlParamsToSanitize){
				int paramIndex = url.indexOf(param);
				if(paramIndex != -1){
					int valueBoundary = url.indexOf('&', paramIndex);
					String value = url.substring(paramIndex+param.length(), valueBoundary!=-1?valueBoundary:url.length()); 
					url = url.replace(value, "");	
				}
			}
		}
		return url;
	}
}
