package util;

/**
 * OAuth actions that map to URLs to fetch from external services.
 */
public enum OauthAction {
	OAUTH_LINK, // get the OAuth Link for a given service
	ACCESS_TOKEN; // get the Access tokens for a given service
}
