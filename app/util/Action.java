package util;

/**
 * Controller actions that map to URLs to fetch from external services.
 */
public enum Action {
	PROFILE, // get the profile and timeline
	SEARCH, // search for profiles matching certain terms
	QUERY; // execute any api query for a given social data provider endpoint
}
