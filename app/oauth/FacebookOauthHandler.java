package oauth;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Map;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FacebookApi;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;
import org.scribe.utils.URLUtils;

import play.Play;
import play.libs.WS;
import play.libs.WS.WSRequest;
import play.mvc.Scope.Params;
import services.FacebookService;
import util.ConnectionUtil;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

public class FacebookOauthHandler extends BaseOAuthHandler {
    // request extended permissions to view additional profile details
    private static final String FACEBOOK_SCOPE = "offline_access,friends_about_me,friends_activities,friends_birthday,friends_education_history,friends_events,friends_groups,friends_hometown,friends_interests,friends_location,friends_notes,friends_online_presence,friends_relationships,friends_relationship_details,friends_religion_politics,friends_status,friends_website,friends_work_history";
    private static final String FACEBOOK_ACCESS_URL = "https://graph.facebook.com/oauth/access_token";
    private static final String FACEBOOK_OAUTH_URL = "https://www.facebook.com/dialog/oauth";

    @Override
    public Token getAccessToken(Params params) throws IOException {
        String oauthToken = params.get("oauthToken");
        String originalUrl = params.get("originalUrl");
        ImmutableMap<String, String> queryParams = ImmutableMap.of("client_id",
                FacebookService.FACEBOOK_APP_ID, "redirect_uri", originalUrl,
                "client_secret", FacebookService.FACEBOOK_APP_SECRET, "code", oauthToken);

        String accessTokenUrl = URLUtils.appendParametersToQueryString(FACEBOOK_ACCESS_URL, queryParams);

        HttpURLConnection connection = ConnectionUtil.createConnection(accessTokenUrl);
        BufferedReader urlConnectionOutput = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String accessTokenResponse = urlConnectionOutput.readLine();
        urlConnectionOutput.close();

        if (accessTokenResponse == null) {
            return new Token("none", "none");
        } else {
            String oauthParam;
            final Splitter responseSplitter = Splitter.on('&').trimResults()
                    .omitEmptyStrings();
            ArrayList<String> responses = Lists.newArrayList(responseSplitter
                    .split(accessTokenResponse));
            oauthParam = responses.get(0);
            final Splitter oauthSplitter = Splitter.on('=').trimResults()
                    .omitEmptyStrings();
            ArrayList<String> oauthParams = Lists.newArrayList(oauthSplitter
                    .split(oauthParam));
            String tokenvalue = oauthParams.get(1);
            return new Token(tokenvalue, "none");
        }
    }

    @Override
    public String getOAuthLinkUrl(Params params)
            throws MalformedURLException {
        String callbackUrl = params.get("currentUrl");
        ImmutableMap<String, String> queryParams = ImmutableMap.of("client_id",
                FacebookService.FACEBOOK_APP_ID, "redirect_uri", callbackUrl,
                "scope", FACEBOOK_SCOPE);
        return URLUtils.appendParametersToQueryString(FACEBOOK_OAUTH_URL, queryParams);
    }
}
