package oauth;

import java.io.IOException;
import java.net.MalformedURLException;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import play.Play;
import play.mvc.Scope.Params;
import services.TwitterService;

public class TwitterOauthHandler extends BaseOAuthHandler {
	private ServiceBuilder serviceBuilder = new ServiceBuilder()
			.provider(TwitterApi.class).apiKey(TwitterService.TWITTER_API_KEY)
			.apiSecret(TwitterService.TWITTER_API_SECRET);

	@Override
	public Token getAccessToken(Params params) throws MalformedURLException,
			IOException {
		String oauth_verifier = params.get("oauth_verifier");
		String oauth_token = params.get("oauth_token");
		Verifier verifier = new Verifier(oauth_verifier);
		// Twitter doesn't actually check the signature, otherwise we'd have to
		// keep the secret around like LinkedIn.
		OAuthService service = serviceBuilder.build();
		return service.getAccessToken(new Token(oauth_token, ""), verifier);
	}

	@Override
	public String getOAuthLinkUrl(Params params) throws MalformedURLException {
		String callbackUrl = params.get("currentUrl");
		OAuthService serviceWithCustomCallback = serviceBuilder.callback(
				callbackUrl).build();
		return serviceWithCustomCallback
				.getAuthorizationUrl(serviceWithCustomCallback
						.getRequestToken());
	}

}
