package oauth;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import org.scribe.model.*;

import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;
import play.mvc.Scope.Params;
import play.mvc.results.RenderJson;
import util.OauthAction;

public abstract class BaseOAuthHandler {

	/**
     * Returns the token to be stored in the database that can be used for authenticated calls to the social provider.
     * 
     * @param params The parameter map of the incoming request
     * @return An access Token that should be stored in the database.
     * @throws MalformedURLException
     * @throws IOException
     */
    public abstract Token getAccessToken(Params params) throws MalformedURLException, IOException;
    
    /**
     * Gets the link to the display to the user in order to initiate the oauth flow.
     * 
     * @return a link to show the user
     */
    public abstract String getOAuthLinkUrl(Params params) throws MalformedURLException;


	public Object performAction(OauthAction action, Params params) throws IOException {
		switch (action) {
			case ACCESS_TOKEN:
				return getAccessToken(params);
			case OAUTH_LINK:
				return getOAuthLinkUrl(params);
			default:
				throw new RuntimeException("Invalid Action");
		}
	}

	public Map renderOauthResponse(OauthAction action, Object response) {
		Map<String, String> jsonMap = null;
		switch (action) {
		case ACCESS_TOKEN:
			Token OauthToken = (Token) response;
			jsonMap = ImmutableMap.of("secret", OauthToken.getSecret(),
					"token", OauthToken.getToken());
			return jsonMap;
		case OAUTH_LINK:
			String OauthLinkUrl = String.valueOf(response);
			jsonMap = ImmutableMap.of("callbackUrl", OauthLinkUrl);
			return jsonMap;
		default:
			throw new RuntimeException("Invalid Action");
		}
	}
}
