package oauth;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.LinkedInApi;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import play.Play;
import play.cache.Cache;
import play.mvc.Scope.Params;
import services.LinkedInService;

public class LinkedInOauthHandler extends BaseOAuthHandler {
    private ServiceBuilder serviceBuilder = new ServiceBuilder().provider(LinkedInApi.class)
                                                                .apiKey(LinkedInService.CONSUMER_KEY)
                                                                .apiSecret(LinkedInService.CONSUMER_SECRET);

    @Override
    public Token getAccessToken(Params params) {
        
    	String oauthVerifier = params.get("oauth_verifier");
		String oauthToken = params.get("oauthToken");
    	
        Verifier verifier = new Verifier(oauthVerifier);
        Token requestToken = new Token(oauthToken, String.valueOf(Cache.get(oauthToken)));
        Cache.safeDelete(oauthToken);
        
        OAuthService service = serviceBuilder.build();
        return service.getAccessToken(requestToken, verifier);
    }

	public String getOAuthLinkUrl(Params params) throws MalformedURLException {
		
		String callbackUrl = params.get("currentUrl");
		OAuthService serviceWithCustomCallback = serviceBuilder.callback(
				callbackUrl).build();

		Token requestToken = serviceWithCustomCallback.getRequestToken();
		Cache.safeSet(requestToken.getToken(),
				requestToken.getSecret(),"30mn");
		return serviceWithCustomCallback.getAuthorizationUrl(requestToken);
		
	}

}
