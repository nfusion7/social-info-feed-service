package services;
import java.util.Map;

import play.Play;
import play.libs.WS;
import play.mvc.Scope.Params;
import util.Action;

import com.google.common.collect.ImmutableMap;

public class FacebookService extends Service {
    public static final String FACEBOOK_APP_ID = Play.configuration.getProperty("facebook.app.id");;
    public static final String FACEBOOK_APP_SECRET = Play.configuration.getProperty("facebook.app.secret");;

	private static final String SERVER_DOMAIN = Play.configuration.getProperty("facebook.server.domain","https://graph.facebook.com/");
    /**
     * The unique identifier for this service.
     */
    public static final String IDENTIFIER = "fb";
    
    private static final String ACCESS_TOKEN = IDENTIFIER + "OauthToken";
    private static final String USER_IDENTIFIER = IDENTIFIER + "UserIdentifier";
    private static final String USER_SEARCH_BASE_URL = SERVER_DOMAIN + "search?type=user&fields=id,name,picture,education,work,location";
    private static final String PAGE_SEARCH_BASE_URL = SERVER_DOMAIN + "search?type=page";

    /**
     * A map of controller action names to a string array of required GET
     * parameters that must be present for that action.
     */
    public static final Map<Action, String[]> REQUIRED_PARAMETERS = ImmutableMap.of(
 	   Action.PROFILE, new String[] { ACCESS_TOKEN, USER_IDENTIFIER },
	   Action.SEARCH, new String[] { ACCESS_TOKEN, SEARCH_TERM },
	   Action.QUERY, new String[] { ACCESS_TOKEN, API_QUERY_URL }
	);

    public FacebookService(){
    	super(REQUIRED_PARAMETERS, IDENTIFIER);
    }
    
    /**
     * Returns a known map of URLs to get data from for this particular service
     * given a list of service-specific parameters from the external request.
     * @param params all the parameters in the combined request
     * @return a map of result names to the URLs to get the data from 
     */
    @Override
    public Map<String, ServiceUrlFuture> getUrls(Action actionName, Params params) {
    	String accessToken = params.get(ACCESS_TOKEN);
    	switch (actionName) {
    		case PROFILE:
    			String userIdentifier = params.get(USER_IDENTIFIER);
    			return ImmutableMap.of(
    			    Service.USER_INFO,
                        new ServiceUrlFuture(SERVER_DOMAIN + userIdentifier + "?return_ssl_resources=1&access_token=" + WS.encode(accessToken)),
    			    Service.USER_TIMELINE,
                        new ServiceUrlFuture(SERVER_DOMAIN + userIdentifier + "/posts?return_ssl_resources=1&access_token=" + WS.encode(accessToken)));
    		case SEARCH:
    			String searchTerm = params.get(SEARCH_TERM);              
    			String searchType = params.get(SEARCH_TYPE_PARAM);

    			String url = null;
    			if(searchType == null || searchType.equals(SEARCH_TYPE_USER)) {
    				url = USER_SEARCH_BASE_URL;
    			} else if(searchType.equals(SEARCH_TYPE_BUSINESS)) {
    				url = PAGE_SEARCH_BASE_URL;
    			} else if(searchType.equals(SEARCH_TYPE_FEEDITEM)) {
    				return Service.getEmptyServiceFutureMap(Service.USER_SEARCH);
    			}
    			return ImmutableMap.of(
    			    Service.USER_SEARCH,
                        new ServiceUrlFuture(url + "&access_token=" + WS.encode(accessToken) + "&q=" + WS.encode(searchTerm)));
    		case QUERY:
    			String apiQueryUrl = params.get(API_QUERY_URL);
    			return ImmutableMap.of(
        			    Service.API_QUERY_RESULT,
                            new ServiceUrlFuture(apiQueryUrl + "&access_token=" + WS.encode(accessToken)));
    	}
    	throw new UnsupportedOperationException("ActionName not supported: " + actionName);
    }
}
