package services;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.LinkedInApi;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;
import org.scribe.utils.URLUtils;

import play.Play;
import play.libs.WS;
import play.mvc.Scope.Params;
import util.Action;

import com.google.common.collect.ImmutableMap;

public class LinkedInService extends Service {
    /**
     * API credentials for application: SFDC Development
     */
    public static final String CONSUMER_KEY = Play.configuration.getProperty("linkedin.key");
    public static final String CONSUMER_SECRET = Play.configuration.getProperty("linkedin.secret");

    private static final String SERVER_DOMAIN = Play.configuration.getProperty("linkedin.server.domain","https://api.linkedin.com/v1/");

    /**
     * The unique identifier for this service.
     */
    public static final String IDENTIFIER = "li";

    private static final String OAUTH_TOKEN = IDENTIFIER + "OauthToken";
    private static final String OAUTH_SECRET = IDENTIFIER + "OauthSecret";
    private static final String USER_IDENTIFIER = IDENTIFIER + "UserIdentifier";

    /**
     * Constants that store the headers we need to pass in, in order to get JSON data (instead of XML).
     */
    private static final String JSON_HEADER_NAME = "x-li-format";
    private static final String JSON_HEADER_VALUE = "json";
    private static final Map<String, String> ADDITIONAL_HEADERS = ImmutableMap.of(JSON_HEADER_NAME, JSON_HEADER_VALUE);

    /**
     * The list of required fields from LinkedIn's API, which needs to be explicitly expressed in the request.
     */
    private static final String[] PROFILE_FIELDS_ARRAY = new String[] {
        "id", "first-name", "last-name", "public-profile-url", "location:(name)", "headline", "picture-url",
        "educations", "positions", "relation-to-viewer:(num-related-connections,related-connections,distance)",
        "distance", "site-standard-profile-request:(url)"
    };


    private static final String[] PUBLIC_PROFILE_FIELDS_ARRAY = new String[] {
        "id", "first-name", "last-name", "public-profile-url", "location:(name)", "headline", "picture-url",
        "educations", "positions"
    };


    private static final String PROFILE_FIELDS_STRING = ":(" + StringUtils.join(PROFILE_FIELDS_ARRAY, ",") + ")";

    private static final String PUBLIC_PROFILE_FIELDS_STRING = ":public:(" + StringUtils.join(PUBLIC_PROFILE_FIELDS_ARRAY, ",") + ")";

    private static final String SEARCH_BASE_URL = SERVER_DOMAIN + "people-search:(people:(id,first-name,last-name," +
    		"headline,location:(name),educations,positions,picture-url),num-results)?sort=relevance&keywords=";

    /**
     * A map of controller action names to a string array of required GET
     * parameters that must be present for that action.
     */
    public static final Map<Action, String[]> REQUIRED_PARAMETERS = ImmutableMap.of(
 	   Action.PROFILE, new String[] { OAUTH_TOKEN, OAUTH_SECRET, USER_IDENTIFIER },
	   Action.SEARCH, new String[] { OAUTH_TOKEN, OAUTH_SECRET, SEARCH_TERM },
	   Action.QUERY, new String[] { OAUTH_TOKEN, OAUTH_SECRET, API_QUERY_URL }
    );


    private OAuthService oauthService;

    public LinkedInService() {
    	super(REQUIRED_PARAMETERS, IDENTIFIER);

        // Create the singleton OAuth service.
        oauthService = new ServiceBuilder()
                              .provider(LinkedInApi.class)
                              .apiKey(CONSUMER_KEY)
                              .apiSecret(CONSUMER_SECRET)
                              .build();
    }

    /**
     * Returns a known map of URLs to get data from for this particular service
     * given a list of service-specific parameters from the external request.
     * @param params all the parameters in the combined request
     * @return a map of result names to the URLs to get the data from
     */
    @Override
    public Map<String, ServiceUrlFuture> getUrls(Action actionName, Params params) {
    	switch(actionName) {
    		case PROFILE:
    			String userIdentifier = params.get(USER_IDENTIFIER);
    			String personUrlPrefix;
    			if(isEmailIdentifier(userIdentifier)) {
    				personUrlPrefix = SERVER_DOMAIN + "people/email=" +
    					stripPrefixForEmailIdentifier(userIdentifier);
    			} else if(isUrlIdentifier(userIdentifier)) {
    				String encodedUrl = URLUtils.formURLEncode(userIdentifier);
    				personUrlPrefix = SERVER_DOMAIN + "people/url=" + encodedUrl;
    			} else {
    				personUrlPrefix = SERVER_DOMAIN + "people/id=" + userIdentifier;
    			}
    			if(params._contains("getPublicProfile")){
    				return ImmutableMap.of(Service.USER_INFO,new ServiceUrlFuture(personUrlPrefix + PUBLIC_PROFILE_FIELDS_STRING));
    			} else {
	                return ImmutableMap.of(
	    			    Service.USER_INFO, new ServiceUrlFuture(personUrlPrefix + PROFILE_FIELDS_STRING ));  // Limit to 20
    			}
    		case SEARCH:
    			String searchTerm = params.get(SEARCH_TERM);
    			String searchType = params.get(SEARCH_TYPE_PARAM);
    			String url = null;
    			if(searchType == null || searchType.equals(SEARCH_TYPE_USER)) {
    				url = SEARCH_BASE_URL;
    			} else if(searchType.equals(SEARCH_TYPE_BUSINESS)) {
    				return Service.getEmptyServiceFutureMap(Service.USER_SEARCH);
    			} else if(searchType.equals(SEARCH_TYPE_FEEDITEM)) {
    				return Service.getEmptyServiceFutureMap(Service.USER_SEARCH);
    			}
    			// TODO: could include &company-name= below.
    			// company name is not necessarily their current company, so we'll still find them if they change jobs
    			return ImmutableMap.of(
                    Service.USER_SEARCH,
                        new ServiceUrlFuture(url + WS.encode(searchTerm)));
			case QUERY:
				String apiQueryUrl = params.get(API_QUERY_URL);
    			return ImmutableMap.of(
        			    Service.API_QUERY_RESULT,
                            new ServiceUrlFuture(apiQueryUrl));
    	}

    	throw new UnsupportedOperationException("ActionName not supported: " + actionName);
    }

    /**
     * Whether this service needs Oauth.
     */
    @Override
    public Boolean needsOauth() {
        return true;
    }

    /**
     * Gets the OAuthService singleton for accessing this service's API.
     * @return singleton OAuthService for this service
     */
    @Override
    public OAuthService getOAuthService() {
        return oauthService;
    }

    /**
     * Gets the OAuth token required for this service.
     * @param params input request parameters
     * @return the OAuth token object
     */
    @Override
    public Token getToken(Params params) {
    	return new Token(params.get(OAUTH_TOKEN), params.get(OAUTH_SECRET));
    }

    /**
     * Returns additional headers needed for LinkedIn to return JSON responses.
     *
     * @return additional headers needed for LinkedIn
     */
    @Override
    public Map<String, String> getAdditionalRequestHeaders() {
        return ADDITIONAL_HEADERS;
    }
}
