package services;

import java.util.concurrent.Future;

/**
 * External service URL struct that contains what the URL string is,
 * the future for it and whether to wrap the response.
 *
 * This class is mutable.
 */
public class ServiceUrlFuture {
    private String url;
    private boolean wrapWithData;
    private Future future;

    /**
     * Default constructor that does not wrap with data.
     * @param url The external service url
     */
    public ServiceUrlFuture(String url)
    {
        this(url, false);
    }

    /**
     * Constructor that takes a boolean to decide whether to wrap the data or not/
     * @param url The external service url
     * @param wrapWithData Whether to wrap with a data fragment
     */
    public ServiceUrlFuture(String url, boolean wrapWithData)
    {
        this.url = url;
        this.wrapWithData = wrapWithData;
    }

    /**
     * Sets the future for this.
     * @param future
     */
    public void setFuture(Future future) {
        this.future = future;
    }

    /**
     * @return future
     */
    public Future getFuture() {
        return future;
    }

    /**
     * @return wrapWithData
     */
    public boolean getWrapWithData() {
        return wrapWithData;
    }

    /**
     * @return url
     */
    public String getUrl() {
        return url;
    }
}