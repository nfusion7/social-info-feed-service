package services;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

import play.Play;
import play.libs.WS;
import play.mvc.Scope.Params;
import util.Action;

public class YouTubeService extends Service {

	public static final String IDENTIFIER = "yt";

	private static final Map<Action, String[]> REQUIRED_PARAMETERS = ImmutableMap.of(
			Action.SEARCH, new String[] { SEARCH_TERM }	
	);	

	private static String DEVELOPER_KEY = Play.configuration.getProperty("youtube.key");

	private static String SEARCH_BASE_URL = "https://gdata.youtube.com/feeds/api/videos?alt=json&max-results=25&v=2&start-index=1" +
	"&key=" + DEVELOPER_KEY + "&q=";

	// user info provides information on youtube user, such as num videos uploaded
	// private String USER_INFO_BASE_URL = "https://gdata.youtube.com/feeds/api/users/USERNAME_HERE?alt=json";

	public YouTubeService(){
		super(REQUIRED_PARAMETERS, IDENTIFIER);
	}

	/**
	 * Returns a known map of URLs to get data from for this particular service
	 * given a list of service-specific parameters from the external request.
	 * @param params all the parameters in the combined request
	 * @return a map of result names to the URLs to get the data from 
	 */
	@Override
	public Map<String, ServiceUrlFuture> getUrls(Action actionName, Params params) {
		String searchTerm = params.get(SEARCH_TERM);
		switch (actionName) {
		case SEARCH:
			String url = SEARCH_BASE_URL + WS.encode(searchTerm);

			String orderBy = params.get(ORDER_BY);
			if(orderBy != null) {
				url += "&orderby=" + WS.encode(orderBy);
			} else {
				url += "&orderby=published";
			}

			return ImmutableMap.of(
					Service.USER_SEARCH,
					new ServiceUrlFuture(url));
		}
		throw new UnsupportedOperationException("ActionName not supported: " + actionName);
	}

}
