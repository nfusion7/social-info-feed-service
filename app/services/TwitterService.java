package services;
import java.util.Map;

import oauth.TwitterOauthHandler;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import play.Play;
import play.libs.WS;
import play.mvc.Scope.Params;
import util.Action;

import com.google.common.collect.ImmutableMap;

public class TwitterService extends Service {
    public static String TWITTER_API_KEY = Play.configuration.getProperty("twitter.key");
    public static String TWITTER_API_SECRET = Play.configuration.getProperty("twitter.secret");

	private static final String SERVER_DOMAIN = Play.configuration.getProperty("twitter.server.domain","https://api.twitter.com/1.1/");
	private static final String SEARCH_BASE_DOMAIN = Play.configuration.getProperty("twitter_search.server.domain","http://search.twitter.com/");
	
	/**
	 * The unique identifier for this service.
	 */
	public static final String IDENTIFIER = "tw";

	private static final String OAUTH_TOKEN = IDENTIFIER + "OauthToken";
	private static final String OAUTH_SECRET = IDENTIFIER + "OauthSecret";
	private static final String USER_IDENTIFIER = IDENTIFIER + "UserIdentifier";
	
	// per_page GET variable below determines number of twitter search results
	private static final String USER_SEARCH_BASE_URL = SERVER_DOMAIN + "users/search.json?count=20&q=";
	// rpp stands for results per page
	private static final String TWEET_SEARCH_BASE_URL = SEARCH_BASE_DOMAIN + "/search.json?rpp=25&q=";

	/**
	 * A map of controller action names to a string array of required GET
	 * parameters that must be present for that action.
	 */
    public static final Map<Action, String[]> REQUIRED_PARAMETERS = ImmutableMap.of(
		Action.PROFILE, new String[] { OAUTH_TOKEN, OAUTH_SECRET, USER_IDENTIFIER },
		// TODO: make token and secret required for user search but not tweet search
		Action.SEARCH, new String[] { SEARCH_TERM },
		Action.QUERY, new String[] { OAUTH_TOKEN, OAUTH_SECRET,API_QUERY_URL }
	);

	private OAuthService oauthService;

	public TwitterService() {
		super(REQUIRED_PARAMETERS, IDENTIFIER);

		// Create the singleton OAuth service.
		oauthService = new ServiceBuilder()
		.provider(TwitterApi.class)
		.apiKey(TWITTER_API_KEY)
		.apiSecret(TWITTER_API_SECRET)
		.build();
	}

	/**
	 * Returns a known map of URLs to get data from for this particular service
	 * given a list of service-specific parameters from the external request.
	 * @param params all the parameters in the combined request
	 * @return a map of result names to the URLs to get the data from
	 */
	@Override
	public Map<String, ServiceUrlFuture> getUrls(Action actionName, Params params) {
		switch(actionName) {
			case PROFILE:
				String userIdentifier = params.get(USER_IDENTIFIER);
				return ImmutableMap.of(
				    Service.USER_INFO,
                        new ServiceUrlFuture(SERVER_DOMAIN + "users/show.json?screen_name=" + userIdentifier),
				    Service.USER_TIMELINE,
                        new ServiceUrlFuture(SERVER_DOMAIN + "statuses/user_timeline.json?count=20&screen_name=" + userIdentifier, true /* wrapWithData */));
			case SEARCH:
				String searchTerm = params.get(SEARCH_TERM);
    			String searchType = params.get(SEARCH_TYPE_PARAM);
    			
    			String url = null;
    			if(searchType == null || searchType.equals(SEARCH_TYPE_USER)) {
    				url = USER_SEARCH_BASE_URL;
    			} else if(searchType.equals(SEARCH_TYPE_BUSINESS)) {
    				url = USER_SEARCH_BASE_URL;
    			} else if(searchType.equals(SEARCH_TYPE_FEEDITEM)) {
    				url = TWEET_SEARCH_BASE_URL;
    			}
				// per_page determines the number of tweets to retrieve for each search result
				// we don't need any tweets with our results as we'll be making a separate call
                return ImmutableMap.of(
				    Service.USER_SEARCH,
                        new ServiceUrlFuture(url + WS.encode(searchTerm), true /* wrapWithData */));
			case QUERY:
				String apiQueryUrl = params.get(API_QUERY_URL);
    			return ImmutableMap.of(
        			    Service.API_QUERY_RESULT,
                            new ServiceUrlFuture(apiQueryUrl));
		}
		throw new UnsupportedOperationException("ActionName not supported: " + actionName);
	}

	/**
	 * Whether this service needs Oauth.
	 */
	@Override
	public Boolean needsOauth(Action actionName, Params params) {
		String searchType = params.get(SEARCH_TYPE_PARAM);
		if(actionName == Action.SEARCH && searchType != null && searchType.equals(SEARCH_TYPE_FEEDITEM)) {
			return false;
		}
		return true;
	}

	/**
	 * Gets the OAuthService singleton for accessing this service's API.
	 * @return singleton OAuthService for this service
	 */
	@Override
	public OAuthService getOAuthService() {
		return oauthService;
	}

	/**
	 * Gets the OAuth token required for this service.
	 * @param params input request parameters
	 * @return the OAuth token object
	 */
	@Override
	public Token getToken(Params params) {
		return new Token(params.get(OAUTH_TOKEN), params.get(OAUTH_SECRET));
	}
}
