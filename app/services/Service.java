package services;

import java.util.Map;
import java.util.Set;

import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import play.mvc.Scope.Params;
import util.Action;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

public abstract class Service {
	protected final static String USER_INFO = "userInfo";
	protected final static String USER_TIMELINE = "userTimeline";
	protected final static String USER_SEARCH = "userSearch";
	protected final static String API_QUERY_RESULT = "apiQueryResult";

	/**
	 * The GET parameter from which we retrieve the term we're searching for.
	 */
	protected static final String SEARCH_TERM = "searchTerm";

	/**
	 * The GET parameter from which we retrieve the order for search results.
	 */
	protected static final String ORDER_BY = "orderBy";
	/**
	 * The GET parameter to specify the type of search being run
	 */
	protected static final String SEARCH_TYPE_PARAM = "type";

	/**
	 * The GET value for search type that means do a user search.
	 */
	protected static final String SEARCH_TYPE_USER = "user";

	/**
	 * The GET value for search type that means do a feed-item search.
	 */
	protected static final String SEARCH_TYPE_FEEDITEM = "feed_item";

	/**
	 * The GET value for search type that means do a business search.
	 */
	protected static final String SEARCH_TYPE_BUSINESS = "business";

	/**
	 * The GET parameter for which api query to call
	 */
	protected static final String API_QUERY_URL = "apiQueryUrl";

	/**
	 * A prefix on the user identifier that indicates this is an email address.
	 */
	private static final String USER_IDENTIFIER_EMAIL_PREFIX = "email:";

	/**
	 * Singleton empty headers map that can be used as the default for all services.
	 */
	public final static Map<String, String> EMPTY_HEADERS = ImmutableMap.of();

	private String identifier;

	/**
	 * Maps a controller action name (e.g. "search") to a String
	 * array of GET parameters that must be set.
	 */
	private Map<Action, String[]> requiredParameters;

	public Service(Map<Action, String[]> requiredParameters, String identifier) {
		this.requiredParameters = requiredParameters;
		this.identifier = identifier;
	}

	public String getIdentifier() {
		return identifier;
	}

	public String[] getRequiredParameters(Action actionName){
		return requiredParameters.get(actionName);
	}

	/**
	 * Returns a known map of URLs to get data from for this particular service
	 * given a list of service-specific parameters from the external request.
	 * @param actionName the controller function for which urls are requested
	 * @param params all the parameters in the combined request
	 * @return a map of result names to the URLs to get the data from
	 */
	public abstract Map<String, ServiceUrlFuture> getUrls(Action actionName, Params params);

	/**
	 * Checks to see if the request parameters has all the required specific
	 * parameters required for this service.
	 * @param actionName the name of the controller action whose required params
	 * to check for
	 * @param params all the parameters in the combined request
	 * @return true iff all service-specific parameters exist
	 */
	public Boolean hasValidParams(Action actionName, Params params) {
		String[] requiredParams = getRequiredParameters(actionName);
		if(requiredParams == null) {
			throw new UnsupportedOperationException(
					"ActionName " + actionName + " not supported" +
					" for service: " + this.identifier);
		}
		for (String param : requiredParams) {
			String p = params.get(param);
			if (p == null || p.trim().length() == 0) {
				return false;
			}
		}
		if(actionName.equals(Action.QUERY)){
			return isValidEndpoint(params.get(API_QUERY_URL));
		}
		return true;
	}
	//TODO: remove twitter v1 endpoint once socialcrm app code is upgraded to v1.1
	private static final Set<String> validEndpoints = ImmutableSet.<String> of(
			"https://graph.facebook.com/", "https://api.linkedin.com/v1/",
			"https://api.twitter.com/1/", "https://api.twitter.com/1.1/",
			"https://api.facebook.com/","https://www.facebook.com/",
			"http://api.klout.com/v2/");

	public static boolean isValidEndpoint(final String url) {
		for (String s : validEndpoints) {
			if (url.startsWith(s))
				return true;
		}
		return false;
	}

	public Boolean needsOauth(Action actionName, Params params) {
		return needsOauth();
	}

	/**
	 * Whether this service needs Oauth.
	 */
	public Boolean needsOauth() {
		return false;
	}

	/**
	 * Gets the OAuthService singleton for accessing this service's API.
	 * @return singleton OAuthService for this service
	 */
	public OAuthService getOAuthService() throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	/**
	 * Gets the OAuth token required for this service.
	 * @param params input request parameters
	 * @return the OAuth token object
	 */
	public Token getToken(Params params) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	/**
	 * Returns any additional headers needed for each request to the service's
	 * API endpoint, e.g. LinkedIn's headers to ask the responses to be returned
	 * as JSON.
	 *
	 * @return a non-null Map of header names and values
	 */
	public Map<String, String> getAdditionalRequestHeaders() {
		return EMPTY_HEADERS;
	}

	/**
	 *  This method returns a service - future map that has a future that returns immediately
	 *  with no returned value. This is useful e.g. to retrieve a future map for unimplemented or irrelevant
	 *  service requests, such as a search type that doesn't make sense for a particular provider.
	 * @param service
	 * @return a map from the service to an empty future that will fire immediately
	 *   with no returned value.
	 */
	public static Map<String, ServiceUrlFuture> getEmptyServiceFutureMap(String service) {
		ServiceUrlFuture serviceMap = new ServiceUrlFuture(null);
		return ImmutableMap.of(
				service,
				serviceMap
		);
	}

	/**
	 *  This method takes a user identifier and returns a boolean indicating
	 *  whether or not this user identifier is intended to be used for an email
	 *  address lookup.
	 */
	protected static Boolean isEmailIdentifier(String userIdentifier) {
		return userIdentifier.startsWith(USER_IDENTIFIER_EMAIL_PREFIX);
	}

	/**
	 *  This method takes a user identifier and returns a boolean indicating
	 *  whether or not this user identifier is intended to be used for a URL
	 *  lookup.
	 */
	protected static final Boolean isUrlIdentifier(String userIdentifier) {
		return userIdentifier.startsWith("http://") || userIdentifier.startsWith("https://");
	}

	/**
	 * This method strips the prefix from the user identifier used to
	 * indicate that this user identifier contains an email address
	 * intended for an email address lookup.
	 * @return
	 */
	protected static String stripPrefixForEmailIdentifier(String userIdentifier) {
		return userIdentifier.substring(USER_IDENTIFIER_EMAIL_PREFIX.length());
	}
}
