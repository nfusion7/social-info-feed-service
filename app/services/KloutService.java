package services;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

import play.Play;
import play.libs.WS;
import play.mvc.Scope.Params;
import util.Action;

public class KloutService extends Service {
	public static final String IDENTIFIER = "kl";

	private static final String USER_IDENTIFIER = IDENTIFIER + "UserIdentifier";
	private static final String API_VERSION = IDENTIFIER + "ApiVersion";

	private static final Map<Action, String[]> REQUIRED_PARAMETERS = ImmutableMap.of(
			Action.PROFILE, new String[] { USER_IDENTIFIER },
			Action.QUERY, new String[] { API_QUERY_URL }
	);

	private static String API_KEY = Play.configuration.getProperty("klout.key");
	private static String API_KEY_V2 = Play.configuration.getProperty("klout.v2.key");
	
	private static String BASE_URL = "http://api.klout.com/1/";
	private static String BASE_URL_V2 = "http://api.klout.com/v2/";


	private static String USER_SHOW = "userShow";
	private static String USER_TOPICS = "userTopics";
	private static String USER_INFLUENCED_BY = "userInfluencedBy";
	private static String USER_INFLUENCER_OF = "userInfluencerOf";
	private static String USER_INFLUENCE = "userInfluence";

	//v1 api
	// users should be followed by a comma separated list of users to fetch	
	private static String USER_SHOW_URL = BASE_URL + "users/show.json?key=" + API_KEY + "&users=";
	private static String USER_TOPICS_URL = BASE_URL + "users/topics.json?key=" + API_KEY + "&users=";
	private static String USER_INFLUENCED_BY_URL = BASE_URL + "soi/influenced_by.json?key=" + API_KEY + "&users=";
	private static String USER_INFLUENCER_OF_URL = BASE_URL + "soi/influencer_of.json?key=" + API_KEY + "&users=";

	//v2 api
	private static final String KLOUT_ID_PLACEHOOLDER = "$KLOUT_ID$";
	private static String USER_SHOW_URL_V2 = BASE_URL_V2 + "user.json/"+KLOUT_ID_PLACEHOOLDER+"?key=" + API_KEY_V2;
	private static String USER_TOPICS_URL_V2 = BASE_URL_V2 + "user.json/"+KLOUT_ID_PLACEHOOLDER+"/topics?key=" + API_KEY_V2;
	private static String USER_INFLUENCE_URL_V2 = BASE_URL_V2 + "user.json/"+KLOUT_ID_PLACEHOOLDER+"/influence?key=" + API_KEY_V2;
	
	//v2 api
	public KloutService(){
		super(REQUIRED_PARAMETERS, IDENTIFIER);
	}

	/**
	 * Returns a known map of URLs to get data from for this particular service
	 * given a list of service-specific parameters from the external request.
	 * @param params all the parameters in the combined request
	 * @return a map of result names to the URLs to get the data from 
	 */
	@Override
	public Map<String, ServiceUrlFuture> getUrls(Action actionName, Params params) {
		String userIdentifier = params.get(USER_IDENTIFIER);
		String apiVersion = params.get(API_VERSION);
		
		switch (actionName) {
		case PROFILE:
			String user = WS.encode(userIdentifier);
			if("2".equals(apiVersion)){
				return ImmutableMap.of(
						USER_SHOW, new ServiceUrlFuture(USER_SHOW_URL_V2.replace(KLOUT_ID_PLACEHOOLDER, userIdentifier)),
						USER_TOPICS, new ServiceUrlFuture(USER_TOPICS_URL_V2.replace(KLOUT_ID_PLACEHOOLDER,userIdentifier)),
						USER_INFLUENCE, new ServiceUrlFuture(USER_INFLUENCE_URL_V2.replace(KLOUT_ID_PLACEHOOLDER,userIdentifier))
				);
			}else{
				return ImmutableMap.of(
						USER_SHOW, new ServiceUrlFuture(USER_SHOW_URL + user),
						USER_TOPICS, new ServiceUrlFuture(USER_TOPICS_URL + user),
						USER_INFLUENCED_BY, new ServiceUrlFuture(USER_INFLUENCED_BY_URL + user),
						USER_INFLUENCER_OF, new ServiceUrlFuture(USER_INFLUENCER_OF_URL + user)
				);
			}
		case QUERY:
			String apiQueryUrl = params.get(API_QUERY_URL)+"&key="+API_KEY_V2;
			return ImmutableMap.of(
    			    Service.API_QUERY_RESULT,
                        new ServiceUrlFuture(apiQueryUrl));
		}
		throw new UnsupportedOperationException("ActionName not supported: " + actionName);
	}
}
