/**
 * 
 */
package playoverride;

import play.Logger;
import play.Play;
import play.libs.WS;
import play.libs.WS.WSImpl;
import play.libs.WS.WSRequest;
import play.libs.WS.WSWithEncoding;
import play.libs.ws.WSAsync;
import play.libs.ws.WSUrlFetch;


public class WSFS extends WS {

	private static WSImpl wsImpl = null;

	/**
	 * Singleton configured with default encoding - this one is used
	 * when calling static method on WS.
	 */
	private static WSWithEncodingFS wsWithDefaultEncoding = new WSWithEncodingFS(Play.defaultWebEncoding);

	public static WSRequest url(String url) {
		return wsWithDefaultEncoding.url(url);
	}

	@Override
	public void onApplicationStart() {
		wsWithDefaultEncoding = new WSWithEncodingFS(Play.defaultWebEncoding);
	}

	@Override
	public void onApplicationStop() {
		if (wsImpl != null) {
			wsImpl.stop();
			wsImpl = null;
		}
	}
	public static class WSWithEncodingFS extends WSWithEncoding{

		/**
		 * @param arg0
		 */
		public WSWithEncodingFS(String encoding) {
			super(encoding);
		}

		public WSRequest url(String url) {
			init();
			WSRequest wsReq = wsImpl.newRequest(url, encoding);
			return wsReq;
		}

		private synchronized static void init() {
			if (wsImpl != null) return;
			Logger.trace("Using Async for web service");
			wsImpl = new WSAsyncFS();
		}
	}
}
