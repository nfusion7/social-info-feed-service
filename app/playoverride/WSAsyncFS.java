package playoverride;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import oauth.signpost.AbstractOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.http.HttpRequest;

import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient.BoundRequestBuilder;
import com.ning.http.client.Realm.AuthScheme;
import com.ning.http.client.Realm.RealmBuilder;
import com.ning.http.client.PerRequestConfig;
import com.ning.http.client.Response;

import play.jobs.JobsPlugin;
import play.libs.F.Promise;
import play.libs.OAuth.ServiceInfo;
import play.libs.WS.FileParam;
import play.libs.WS.HttpResponse;
import play.libs.WS.Scheme;
import play.libs.WS.WSRequest;
import play.libs.ws.WSAsync;



/**
 * Extends the play framework WSAsync object for FeedService customizations
 */
public class WSAsyncFS extends WSAsync {
	
	
	@Override
	public WSRequest newRequest(String url, String encoding) {
		return new WSAsyncRequestFS(url, encoding);
	}
	
	public class WSAsyncRequestFS extends WSAsyncRequest {

		/**
		 * @return Returns the url.
		 */
		public String getUrl() {
			return super.url;
		}

		/**
		 * @return Returns the encoding.
		 */
		public String getEncoding() {
			return encoding;
		}

		/**
		 * @return Returns the username.
		 */
		public String getUsername() {
			return username;
		}

		/**
		 * @return Returns the password.
		 */
		public String getPassword() {
			return password;
		}

		/**
		 * @return Returns the scheme.
		 */
		public Scheme getScheme() {
			return scheme;
		}

		/**
		 * @return Returns the body.
		 */
		public Object getBody() {
			return body;
		}

		/**
		 * @return Returns the fileParams.
		 */
		public FileParam[] getFileParams() {
			return fileParams;
		}

		/**
		 * @return Returns the headers.
		 */
		public Map<String, String> getHeaders() {
			return headers;
		}

		/**
		 * @return Returns the parameters.
		 */
		public Map<String, Object> getParameters() {
			return parameters;
		}

		/**
		 * @return Returns the mimeType.
		 */
		public String getMimeType() {
			return mimeType;
		}

		/**
		 * @return Returns the followRedirects.
		 */
		public boolean getFollowRedirects() {
			return followRedirects;
		}

		/**
		 * @return Returns the timeout.
		 */
		public Integer getTimeout() {
			return timeout;
		}

		/**
		 * @return Returns the oauthInfo.
		 */
		public ServiceInfo getOauthInfo() {
			return oauthInfo;
		}

		/**
		 * @return Returns the oauthToken.
		 */
		public String getOauthToken() {
			return oauthToken;
		}

		/**
		 * @return Returns the oauthSecret.
		 */
		public String getOauthSecret() {
			return oauthSecret;
		}

		/**
		 * @param wsAsync
		 * @param url
		 * @param encoding
		 */
		protected WSAsyncRequestFS(WSAsync wsAsync, String url, String encoding) {
			wsAsync.super(url, encoding);
		}

		/**
		 * @param url
		 * @param encoding
		 */
		protected WSAsyncRequestFS(String url, String encoding) {
			super(url, encoding);
		}

		/** Execute a GET request asynchronously. */
		@Override
		public Promise<HttpResponse> getAsync() {
			this.type = "GET";
			sign();
			return execute(prepareGet());
		}

		private Promise<HttpResponse> execute(BoundRequestBuilder builder) {
			try {
				final Promise<HttpResponse> smartFuture = new Promise<HttpResponse>();
				prepare(builder).execute(new AsyncCompletionHandler<HttpResponse>() {
					@Override
					public HttpResponse onCompleted(Response response) throws Exception {
						HttpResponse httpResponse = new HttpAsyncResponse(response);
						smartFuture.invoke(httpResponse);
						return httpResponse;
					}
					@Override
					public void onThrowable(Throwable t) {
						JobsPlugin.executor.submit(new Runnable() {
							public void run() {
								smartFuture.invoke(null);
							}
						});
						throw new RuntimeException(t);
					}
				});

				return smartFuture;
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		private BoundRequestBuilder prepare(BoundRequestBuilder builder) {
			if (this.username != null && this.password != null && this.scheme != null) {
				AuthScheme authScheme;
				switch (this.scheme) {
				case DIGEST: authScheme = AuthScheme.DIGEST; break;
				case NTLM: authScheme = AuthScheme.NTLM; break;
				case KERBEROS: authScheme = AuthScheme.KERBEROS; break;
				case SPNEGO: authScheme = AuthScheme.SPNEGO; break;
				case BASIC: authScheme = AuthScheme.BASIC; break;
				default: throw new RuntimeException("Scheme " + this.scheme + " not supported by the UrlFetch WS backend.");
				}
				builder.setRealm(
						(new RealmBuilder())
						.setScheme(authScheme)
						.setPrincipal(this.username)
						.setPassword(this.password)
						.setUsePreemptiveAuth(true)
						.build()
				);
			}
			for (String key: this.headers.keySet()) {
				builder.addHeader(key, headers.get(key));
			}
			builder.setFollowRedirects(this.followRedirects);
			PerRequestConfig perRequestConfig = new PerRequestConfig();
			perRequestConfig.setRequestTimeoutInMs(this.timeout * 1000);
			builder.setPerRequestConfig(perRequestConfig);
			return builder;
		}

		private WSRequest sign() {
			if (this.oauthToken != null && this.oauthSecret != null) {
				WSOAuthConsumer consumer = new WSOAuthConsumer(oauthInfo, oauthToken, oauthSecret);
				try {
					consumer.sign(this, this.type);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
			return this;
		}


	}

	private static class WSOAuthConsumer extends AbstractOAuthConsumer {

		public WSOAuthConsumer(String consumerKey, String consumerSecret) {
			super(consumerKey, consumerSecret);
		}

		public WSOAuthConsumer(ServiceInfo info, String token, String secret) {
			super(info.consumerKey, info.consumerSecret);
			setTokenWithSecret(token, secret);
		}

		@Override
		protected HttpRequest wrap(Object request) {
			if (!(request instanceof WSRequest)) {
				throw new IllegalArgumentException("WSOAuthConsumer expects requests of type play.libs.WS.WSRequest");
			}
			return new WSRequestAdapter((WSRequest)request);
		}

		public WSRequest sign(WSRequest request, String method) throws OAuthMessageSignerException, OAuthExpectationFailedException, OAuthCommunicationException {
			WSRequestAdapter req = (WSRequestAdapter)wrap(request);
			req.setMethod(method);
			sign(req);
			return request;
		}

		public class WSRequestAdapter implements HttpRequest {

			private WSRequest request;
			private String method;

			public WSRequestAdapter(WSRequest request) {
				this.request = request;
			}

			public Map<String, String> getAllHeaders() {
				return request.headers;
			}

			public String getContentType() {
				return request.mimeType;
			}

			public String getHeader(String name) {
				return request.headers.get(name);
			}

			public InputStream getMessagePayload() throws IOException {
				return null;
			}

			public String getMethod() {
				return this.method;
			}

			private void setMethod(String method) {
				this.method = method;
			}

			public String getRequestUrl() {
				return request.url;
			}

			public void setHeader(String name, String value) {
				request.setHeader(name, value);
			}

			public void setRequestUrl(String url) {
				request.url = url;
			}

		}

	}
}
