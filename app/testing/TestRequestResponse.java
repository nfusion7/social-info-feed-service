package testing;

import org.apache.http.HttpStatus;

/**
 * Simple struct-like holder for request URLs and the responses to be validated against
 * in testing.
 */
public class TestRequestResponse {
    private String name;
    private String requestUrl;
    private String response;
    private int statusCode;
    private boolean wrapWithData;

    /**
     * Constructor with default 200 http status..
     *
     * @param name the name of this request URL
     * @param requestUrl the request URL to expect
     * @param response the response to return for that URL
     */
    public TestRequestResponse(String name, String requestUrl, String response) {
        this(name, requestUrl, response, HttpStatus.SC_OK);
    }

    /**
     * Constructor with default 200 http status and wrapWithData.
     *
     * @param name the name of this request URL
     * @param requestUrl the request URL to expect
     * @param response the response to return for that URL
     * @param wrapWithData whether to wrap with data fragment
     */
    public TestRequestResponse(String name, String requestUrl, String response, boolean wrapWithData) {
        this(name, requestUrl, response, HttpStatus.SC_OK);
        this.wrapWithData = wrapWithData;
    }

    /**
     * Constructor with custom status..
     *
     * @param name the name of this request URL
     * @param requestUrl the request URL to expect
     * @param response the response to return for that URL
     */
    public TestRequestResponse(String name, String requestUrl, String response, int statusCode) {
        this.name = name;
        this.requestUrl = requestUrl;
        this.response = response;
        this.statusCode = statusCode;
    }


    /**
     * Getter for the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Getter for the request URL.
     *
     * @return the request
     */
    public String getRequestUrl() {
        return this.requestUrl;
    }

    /**
     * Getter for response.
     *
     * @return the response
     */
    public String getResponse() {
        return this.response;
    }

    /**
     * Getter for status code.
     *
     * @return the status code
     */
    public int getStatusCode() {
        return this.statusCode;
    }

    /**
     * Getter for wrap with data boolean.
     *
     * @return the wrap with data
     */
    public boolean getWrapWithData() {
        return this.wrapWithData;
    }
}
