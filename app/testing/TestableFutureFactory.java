package testing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;

import junit.framework.Assert;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import controllers.FutureFactory;

import play.Logger;
import play.jobs.Job;
import play.jobs.JobsPlugin;
import play.libs.F.Promise;
import play.libs.WS;
import play.libs.WS.HttpResponse;

/**
 * A mock FutureFactory that has additional hooks for setting up responses
 * for testing.
 */
public class TestableFutureFactory extends FutureFactory {
    private List<String> expectedRequestUrls = new ArrayList<String>();
	private List<WS.HttpResponse> responses = new ArrayList<WS.HttpResponse>();
    private Map<String, String> expectedRequestHeaders = new HashMap<String, String>();
    private boolean expectedOauth = false;

    /**
     * Set HTTP responses to return.
     *
     * @param newResponses an list of new responses
     */
	public void setHttpResponses(List<WS.HttpResponse> newResponses) {
		responses.clear();
        responses.addAll(newResponses);
	}

    /**
     * Add an HTTP response to return.
     *
     * @param requestUrl the request URL expected
     * @param newResponse a new response
     */
	public void addHttpResponse(String requestUrl, WS.HttpResponse newResponse) {
        expectedRequestUrls.add(requestUrl);
		responses.add(newResponse);
	}

    /**
     * Sets up expected request headers to be present in URL requests to this
     * testable future factory (e.g. LinkedIn's JSON headers).
     *
     * These are asserted when the request is called to get the response future
     * and the test will fail fast without them.
     *
     * @param requestHeaders a Map of header names and values
     */
    public void setExpectedRequestHeaders(Map<String, String> requestHeaders) {
        expectedRequestHeaders.clear();
        expectedRequestHeaders.putAll(requestHeaders);
    }


    /**
     * Sets up whether OAuth should be expected for this future factory.
     *
     * @param isOauth whether oauth is expected
     */
    public void setOauthExpected(boolean isOauth) {
        expectedOauth = isOauth;
    }

    /**
     * Overridden method to create a testable future.
     *
     * @param url the url to hit and get data from
     * @return a future HTTP response
     */
    @Override
	public Promise<WS.HttpResponse> createFuture(String url) {
        if (expectedOauth) {
            Assert.fail("This service expects OAuth but requests do not have them enabled.");
        }

        return createFutureHelper(url, null);
    }
    
    /**
     * Overridden method to create a testable future.
     *
     * @param url the url to hit and get data from
     * @param additionalRequestHeaders the map of additional headers to add to the request
     * @return a future HTTP response
     */
    @Override
	public Promise<WS.HttpResponse> createFuture(String url, Map<String, String> additionalRequestHeaders) {
        if (expectedOauth) {
            Assert.fail("This service expects OAuth but requests do not have them enabled.");
        }

        return createFutureHelper(url, additionalRequestHeaders);
	}

    /**
     * Overridden method to create a testable future with OAuth authentication.
     *
     * @param url the url to hit and get data from
     * @return a future HTTP response
     */
    @Override
	public Promise<WS.HttpResponse> createFutureWithOAuth(OAuthService oauthService, Token tokenOb, String url) {
        if (!expectedOauth) {
            Assert.fail("This service does not expects OAuth but requests have them enabled.");
        }

        return createFutureHelper(url, null);
	}

    /**
     * Overridden method to create a testable future with OAuth authentication.
     *
     * @param url the url to hit and get data from
     * @param additionalRequestHeaders the map of additional headers to add to the request
     * @return a future HTTP response
     */
    @Override
	public Promise<HttpResponse> createFutureWithOAuth(
            OAuthService oauthService,
            Token tokenOb,
            String url,
            Map<String, String> additionalRequestHeaders) {
        if (!expectedOauth) {
            Assert.fail("This service does not expects OAuth but requests have them enabled.");
        }

        return createFutureHelper(url, additionalRequestHeaders);
	}

    private Promise<HttpResponse> createFutureHelper(
            String url,
            Map<String, String> additionalRequestHeaders) {

        final Map<String, String> actualRequestHeaders = additionalRequestHeaders;
        final String actualUrl = url;
        
        final Promise<HttpResponse> smartFuture = new Promise<HttpResponse>();
        
        JobsPlugin.executor.submit(new Callable<HttpResponse>() {
            public HttpResponse call() throws Exception {
                for (Map.Entry<String, String> headerEntry : expectedRequestHeaders.entrySet()) {
                    String headerName = headerEntry.getKey();
                    String expectedValue = headerEntry.getValue();
                    String actualValue = actualRequestHeaders.get(headerName);

                    if(expectedValue != actualValue) {
                    	Logger.fatal("Test looked for header value, " + headerName + ", not mocked out. " 
                    			+ "value requested: " + actualValue + " but mocked value is: " + expectedValue);
                    }
                }

                // Assert that the url is correct as expected.
                String expectedUrl = expectedRequestUrls.remove(0);
                if(expectedUrl != actualUrl) {
                	Logger.fatal("Test looked for a URL not mocked out. Url requested: "
                			+ actualUrl + " but mocked url is: " + expectedUrl);
                }
                
                HttpResponse result = responses.remove(0);
                smartFuture.invoke(result);
                return result;
            } 
        });
        return smartFuture;
    }

}