package controllers;

import java.util.Map;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import play.libs.F.Promise;
import play.libs.WS;
import playoverride.WSFS;

public class FutureFactory {
	
	private static String REQUEST_TIMEOUT = "10s"; // 10 seconds

    /**
     * Create an asynchronous future web client service call.
     *
     * @param url the url to hit and get data from
     * @return a future object for this call
     */
	public Promise<WS.HttpResponse> createFuture(String url) {
		return createFuture(url, null);
	}

    /**
     * Create an asynchronous future web client service call with additional request headers.
     *
     * @param url the url to hit and get data from
     * @param additionalRequestHeaders the map of additional headers to add to the request
     * @return a future object for this call
     */
    public Promise<WS.HttpResponse> createFuture(String url, Map<String, String> additionalRequestHeaders) {
        // workaround for timeout bug - use overridden WSFS class for async http call 
    	WS.WSRequest wsRequest = WSFS.url(url);

        if (additionalRequestHeaders != null) {
            if (additionalRequestHeaders != null) {
                addAllHeaders(wsRequest, additionalRequestHeaders);
            }
        }
        wsRequest = wsRequest.timeout(REQUEST_TIMEOUT);

       return wsRequest.getAsync();
    }

    /**
     * Create an asynchronous future web client service call with OAuth support.
     *
     * @param oauthService the OAuth service singleton object to use
     * @param tokenObj the token object authenticating this caller
     * @param url the url to hit and get data from
     * @return a future object for this call
     */
	public Promise<WS.HttpResponse> createFutureWithOAuth(OAuthService oauthService, Token tokenObj, String url) {
        return createFutureWithOAuth(oauthService, tokenObj, url, null);
    }

    /**
     * Create an asynchronous future web client service call with OAuth support.
     *
     * @param oauthService the OAuth service singleton object to use
     * @param tokenObj the token object authenticating this caller
     * @param url the url to hit and get data from
     * @return a future object for this call
     */
	public Promise<WS.HttpResponse> createFutureWithOAuth(
            OAuthService oauthService,
            Token tokenObj,
            String url,
            Map<String, String> additionalRequestHeaders) {
        // We need to do something relatively manual here because the Scribe OAuth library does
        // not work naturally with Play's WS asynchronous request framework. As such, after using
        // Scribe's utilities to sign a request, we then extract out the headers and URL query params manually
        // (either of which will have been modified depending on whether OAuth 1.0 or 2.0 is used)
        // and then use them with WS.url().
        OAuthRequest oauthRequest = new OAuthRequest(Verb.GET, url);
        oauthService.signRequest(tokenObj, oauthRequest);
        // workaround for timeout bug - use overridden WSFS class for async http call 
        WS.WSRequest wsRequest = WSFS.url(oauthRequest.getUrl());

        // Add additional request headers first.
        if (additionalRequestHeaders != null) {
            addAllHeaders(wsRequest, additionalRequestHeaders);
        }

        // Add request headers required by OAuth next.
        addAllHeaders(wsRequest, oauthRequest.getHeaders());

        wsRequest = wsRequest.timeout(REQUEST_TIMEOUT);
        return wsRequest.getAsync();
	}

    /**
     * Adds a header to the WSRequest.
     *
     * @param request the WSRequest to add the header to
     * @param headerName the header name
     * @param headerValue the header value
     */
    private static void addHeader(WS.WSRequest request, String headerName, String headerValue) {
        request.setHeader(headerName, headerValue);
    }

    /**
     * Adds a set of headers to the WSRequest from a map.
     *
     * @param request the WSRequest to add the headers to
     * @param headers a map of header names to header values
     */
    private static void addAllHeaders(WS.WSRequest request, Map<String,String> headers) {
        for (Map.Entry<String,String> kv : headers.entrySet()) {
            addHeader(request, kv.getKey(), kv.getValue());
        }
    }
}