package controllers;

import java.util.List;

import play.mvc.Http;

/**
 * An exception that is the fault of the external 3rd-party service
 * we are pulling results from.  We can catch these "unexpected" exceptions
 * and build logic in our layer for dealing with them appropriately.
 */
public class ExternalServiceException extends Exception {
    private int httpStatus = 0;
    private String responseData;
    private String responseHeaders;
    private static String HEADER_SEPARATOR = " - ";

    /**
     * Constructs an <code>ExternalServiceException</code> with no detail  message.
     */
    public ExternalServiceException () {
    	super();
    }

    /**
     * Constructs an <code>ExternalServiceException</code> with the 
     * specified detail message.
     *
     * @param   s   the detail message.
     */
    public ExternalServiceException (String s) {
	    super(s);
    }

    /**
     * Constructs an <code>ExternalServiceException</code> with the
     * specified detail message and http status code.
     *
     * @param   s   the detail message.
     * @param   httpStatus   the http status
     * @param   responseData   the response data.
     */
    public ExternalServiceException (String s, int httpStatus, String responseData,
    			List<Http.Header> responseHeaders) {
	    super(s);
        this.httpStatus = httpStatus;
        this.responseData = responseData;
        this.responseHeaders = this.headersToString(responseHeaders);
    }
    
    public static String headersToString(List<Http.Header> responseHeaders) {
    	StringBuilder s = new StringBuilder();
    	for (Http.Header h : responseHeaders) {
    		s.append(h.name);
    		s.append(HEADER_SEPARATOR);
    		s.append(h.value());
    		s.append(HEADER_SEPARATOR);
    	}
    	return s.toString();
    }

    /**
     * Returns the HTTP status stored.
     * @return httpStatus
     */
    public int getHttpStatus() {
        return httpStatus;
    }

    /**
     * Returns the HTTP response data stored.
     * @return responseData
     */
    public String getResponseData() {
        return responseData;
    }
    
    public String getResponseHeaders() {
    	return responseHeaders;
    }
    
    public String toString(){
    	String response = super.toString() + " status_code: " + Integer.toString(this.httpStatus) + 
    		" headers: " + this.getResponseHeaders() +
    		" body: " + this.getResponseData();
    	// remove newlines from the string version of the error
    	// for logging purposes
    	return response.replaceAll("\n", " ");
    }
    
    
}
