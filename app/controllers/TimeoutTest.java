package controllers;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;


import play.libs.F;
import play.libs.WS;
import play.mvc.Controller;
import playoverride.WSFS;


public class TimeoutTest extends Controller {
	public static void test() throws Exception{
		String TIME_OUT = "3s";
		//returns data in <10msec
		F.Promise<WS.HttpResponse> promise1 = WSFS.url("http://socialcrm-wsl/linkedin").timeout(TIME_OUT).getAsync();
		 
		// http://my_webserver is designed to return /random2 in 10seconds, which means promise2 will timeout
		F.Promise<WS.HttpResponse> promise2 = WSFS.url("http://socialcrm-wsl/facebook").timeout(TIME_OUT).getAsync();
		
		F.Promise<List<WS.HttpResponse>> promises = F.Promise.waitAll(promise1);
		 
		List<WS.HttpResponse> httpResponses = await(promises); // request gets suspended here
		//Ideally control should reach here after promise2 timeouts and promise1 returns data, but it never reaches here. The promises are in suspend mode.
		renderJSON("{li : "+(promise1.get() == null?"Timeout !!!":promise1.get().getString())+", fb : "+promise2.get().getString()+"}");

	}
}
