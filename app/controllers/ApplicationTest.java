package controllers;

import util.Action;

public class ApplicationTest extends BaseTest {
    /**
     * Renders the combined results of JSON payloads from each requested service.
     * Uses a request parameter to determine which services to look up.
     * The services param is expected to be the form of a lowercase, comma-delimited string
     * of service identifiers.
     */
	public static void profile() {
		performAction(Action.PROFILE);
    }
	
    /**
     * Searches through the services for matching handles based
     * on a query string (which is usually the users' name).
     */
    public static void search() {
    	performAction(Action.SEARCH);
    }
}