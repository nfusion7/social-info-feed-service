package controllers;

import services.TwitterService;
import util.Action;


public class ApplicationPerfTest extends Application{
	/**
	 * Endpoint for performance testing.
     */
	
	public static void profile() {
		addRequiredParams(Action.PROFILE);
		Base.performAction(Action.PROFILE);
    }
	
	public static void search() {
		addRequiredParams(Action.SEARCH);
		Base.performAction(Action.SEARCH);
    }
	

	/**
	 * Add required parameters so that all validations pass
	 * @param action 
	 */
	private static void addRequiredParams(Action action){
		
		params.put(Base.SERVICES_PARAM, TwitterService.IDENTIFIER);
		//add required twitter params
		for(String reqParam: TwitterService.REQUIRED_PARAMETERS.get(action)){
			params.put(reqParam, action.toString());
		}
		if(Action.SEARCH.equals(action)){
			params.put("type", "feed_item");
		}
		
	}
}
