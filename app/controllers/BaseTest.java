package controllers;

import play.Logger;
import play.Play;
import play.mvc.Controller;
import play.mvc.Scope.Params;
import services.Service;
import util.Action;

import com.google.common.base.Splitter;

public class BaseTest extends Controller {
    
    /**
     * The request parameter used to specify a custom status code for the response.
     */
    public final static String STATUS_OVERRIDE_PARAM = "statusOverride";

    /**
     * The request parameter used to specify the Json payload to be returned.
     */
    public final static String JSON_TO_RETURN_PARAM = "jsonToReturn";
    
    /**
     * The default JSON response if none is provided and the /test/ endpoint is hit.
     */
    public final static String DEFAULT_JSON_RESPONSE = "{'tw':{'userInfo':{}},'fb':{'userInfo':{}},'li':{'userInfo':{}}}".replace("'","\"");

    /*
     * A test feedservice for clients to use to test themselves.
     */
    @play.mvc.Util
    public static void performAction(Action action) {
        Logger.debug("URL: " + request.url);
                
        String statusOverride = params.get(STATUS_OVERRIDE_PARAM);
        
        if (statusOverride != null && statusOverride.trim().length() > 0) {
            Integer statusCode = Integer.parseInt(statusOverride);
            response.status = statusCode;
        } else {
            try {
                areParametersValid(action, params);
            } catch (IllegalArgumentException e) {
                renderJSON("{\"globalErrors\":[\"Invalid services parameter string for url\"]}");
            }

            String jsonToReturn = params.get(JSON_TO_RETURN_PARAM);
            if (jsonToReturn == null || jsonToReturn.trim().length() == 0) {
            	jsonToReturn = DEFAULT_JSON_RESPONSE;
            }
            renderJSON(jsonToReturn);
        }
    }
    
    /*
     * Check if all required parameters are present, and if services parameter
     * is set to a valid value. Note: Although this is similar to Application.java's
     * code, it cannot be factored out because Application.index's error() can't be called outside of the
     * main controller method.
     * @param params the parameters to check
     * @throws IllegalArgumentException if the parameters are incorrect
     * @return true if the parameters are correct
     */
    private static void areParametersValid(Action action, Params params) {
        String servicesParam = params.get(Application.SERVICES_PARAM);

        // Fail fast if invalid number of requested services
        if (servicesParam == null ||  servicesParam.trim().length() == 0) {
        	throw new IllegalArgumentException("Invalid number of requested services!");
        }

        Iterable<String> requestedServices = Splitter.on(Application.SERVICES_PARAM_DELIMITER)
                                                .trimResults()
                                                .omitEmptyStrings()
                                                .split(servicesParam);

        for (String serviceIdentifier : requestedServices) {
            // Find out if requested service is known
            Service service = Application.SUPPORTED_SERVICES.get(serviceIdentifier.toLowerCase());

            // Fail fast for various services param errors
            if (service == null) {
                throw new IllegalArgumentException("Unknown service: " + serviceIdentifier);
            } else if (!service.hasValidParams(action, params)) {
                throw new IllegalArgumentException("Missing parameters for serviceIdentifier: " + serviceIdentifier);
            }
        }
    }
}

