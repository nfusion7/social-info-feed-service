package controllers;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import oauth.BaseOAuthHandler;
import oauth.FacebookOauthHandler;
import oauth.LinkedInOauthHandler;
import oauth.TwitterOauthHandler;

import org.apache.http.HttpStatus;
import org.apache.log4j.Level;

import play.Logger;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.F.Promise;
import play.mvc.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import services.*;
import util.Action;
import util.FeedserviceLogger;
import util.OauthAction;

public class Base extends Controller {
	/**
	 * Future factory field which is substituted by tests. TODO: Should find a
	 * way to protect this in production with something like
	 * TestAccessible/PrivateAccessor.
	 */
	public static FutureFactory futureFactory = new FutureFactory();

	/**
	 * Known list of supported services that instantiate singletons API handlers
	 * for each service.
	 */
	public final static ImmutableMap<String, Service> SUPPORTED_SERVICES = ImmutableMap
			.of(FacebookService.IDENTIFIER, new FacebookService(),
					TwitterService.IDENTIFIER, new TwitterService(),
					LinkedInService.IDENTIFIER, new LinkedInService(),
					YouTubeService.IDENTIFIER, new YouTubeService(),
					KloutService.IDENTIFIER, new KloutService());

	/**
	 * The status code used to represent unexpected error.
	 */
	public final static int UNEXPECTED_ERROR_STATUS_CODE = 999;

	/**
	 * The key used to store global errors per request (in request.args).
	 */
	public final static String GLOBAL_ERRORS_KEY = "globalErrors";

	/**
	 * The key used to store service futures per request (in request.args).
	 */
	public final static String FUTURES_KEY = "futures";

	/**
	 * The key used to store request start time in milliseconds. 
	 */
	public final static String REQUEST_START_TIME_KEY = "requestStartTime";
	/**
	 * The services request parameter used to determine which services to load
	 * responses for.
	 */
	public final static String SERVICES_PARAM = "services";

	public final static String SERVICE_PARAM = "service";

	/**
	 * The request uuid param should be a unique identifier that is logged on
	 * both the appserver side and the feedservice side so that we can track
	 * requests as they come across. (it is e.g. unix timestamp + java thread
	 * id)
	 */
	public final static String REQUEST_UUID_PARAM = "request_uuid";

	/**
	 * The delimiter to use to split the services parameter for multiple
	 * services.
	 */
	public final static String SERVICES_PARAM_DELIMITER = ",";

	public final static String CALLBACK_URL = "CallbackUrl";
	
	/**
	 * The default object to place if the json response returns blank. This
	 * string should be a proper json value to make sure we are returning valid
	 * json.
	 */
	public final static String EMPTY_RESPONSE = "\"\"";

	/**
	 * Known list of supported services that instantiate singletons API handlers
	 * for each service.
	 */
	public final static ImmutableMap<String, BaseOAuthHandler> OAUTH_HANDLERS = ImmutableMap
			.of(FacebookService.IDENTIFIER, new FacebookOauthHandler(),
					TwitterService.IDENTIFIER, new TwitterOauthHandler(),
					LinkedInService.IDENTIFIER, new LinkedInOauthHandler());

	/**
	 * Renders the combined results of JSON payloads from each requested
	 * service. Uses a request parameter to determine which services to look up.
	 * The services param is expected to be the form of a lowercase,
	 * comma-delimited string of service identifiers.
	 */
	@play.mvc.Util
	public static void performAction(Action actionName) {
		final long requestStartTime = System.currentTimeMillis();
		request.args.put(REQUEST_START_TIME_KEY, requestStartTime);
		
		final String servicesParam = params.get(SERVICES_PARAM);
		final String requestUuid = params.get(REQUEST_UUID_PARAM);

		// Fail fast if invalid number of requested services
		if (servicesParam == null || servicesParam.trim().length() == 0) {
			addGlobalError(request, "Invalid services parameter string " +
						"for url: " + request.url);
			renderFeedServiceResponse(request, response,
					HttpStatus.SC_BAD_REQUEST);
		}

		Iterable<String> requestedServices = Splitter
				.on(SERVICES_PARAM_DELIMITER).trimResults().omitEmptyStrings()
				.split(servicesParam);

		// Create a list to store a reference to all futures to wait for at the
		// end of the request
		List<Promise> promisesToWaitFor = new LinkedList<Promise>();

		for (String serviceIdentifier : requestedServices) {
			// Find out if requested service is known
			Service service = SUPPORTED_SERVICES.get(serviceIdentifier
					.toLowerCase());

			// Add to global errors for services param errors, and continue.
			if (service == null) {
				addGlobalError(request,
						"Unknown service in services parameter: "
								+ serviceIdentifier);
			} else if (!service.hasValidParams(actionName, params)) {
				addGlobalError(request,
						"Invalid or Missing parameters for requested service: "
								+ serviceIdentifier);
			} else {
				// Get sorted URL mapping.
				Map<String, ServiceUrlFuture> urlsMap = new TreeMap<String, ServiceUrlFuture>(
						service.getUrls(actionName, params));
				Map<String, ServiceUrlFuture> serviceFuturesMap = new TreeMap<String, ServiceUrlFuture>();

				// Create future objects from the URL list defined in the
				// service
				for (Map.Entry<String, ServiceUrlFuture> urlEntry : urlsMap
						.entrySet()) {
					String futureName = urlEntry.getKey();
					ServiceUrlFuture serviceUrlFuture = urlEntry.getValue();

					
					Promise<WS.HttpResponse> future;
					// if there is no URL to fetch we carry on.
					if (serviceUrlFuture.getUrl() == null) {
						continue;
					} else if(service.needsOauth(actionName, params)) {
						future = futureFactory.createFutureWithOAuth(
								service.getOAuthService(),
								service.getToken(params),
								serviceUrlFuture.getUrl(),
								service.getAdditionalRequestHeaders());
					} else {
						future = futureFactory.createFuture(
								serviceUrlFuture.getUrl(),
								service.getAdditionalRequestHeaders());
					}
					// if we're in DEV mode then log every URL we're going to hit
					if(play.Play.mode.isDev()) {
						Logger.info("URL to fetch: " + serviceUrlFuture.getUrl());
					}
					// Store the future for this service URL back to the object.
					serviceUrlFuture.setFuture(future);

					promisesToWaitFor.add(future);
					serviceFuturesMap.put(futureName, serviceUrlFuture);
				}

				// Store the map of futures keyed on the service identifier
				addServiceFuturesMap(request, service.getIdentifier(),
						serviceFuturesMap);
			}
		}

		// Wait for all futures created if any
		if (promisesToWaitFor.size() > 0) {

			// Promise.waitAll((promisesToWaitFor.toArray(new
			// Promise[promisesToWaitFor.size()])));
			Promise<List<WS.HttpResponse>> promises = Promise
					.waitAll(promisesToWaitFor
							.toArray(new Promise[promisesToWaitFor.size()]));

			List<WS.HttpResponse> httpResponses = await(promises);
			// Evented IO restarts here, wait is done.
			FeedserviceLogger.log(Level.INFO,
					requestUuid, request.url, System.currentTimeMillis() - requestStartTime,
						"");
			renderFeedServiceResponse(request, response, HttpStatus.SC_OK);

		} else {
			// Otherwise, it means there are global errors and nothing goes out
			// to external services,
			// so just render the errors, with 400 status code.
			renderFeedServiceResponse(request, response,
					HttpStatus.SC_BAD_REQUEST);
		}

	}

	/**
	 * Store global errors into the list for the request.
	 * 
	 * @param request
	 *            The request being processed
	 * @param errorMessage
	 *            The error message
	 */
	@SuppressWarnings("unchecked")
	private static void addGlobalError(Http.Request request, String errorMessage) {
		if (request.args.get(GLOBAL_ERRORS_KEY) == null) {
			request.args.put(GLOBAL_ERRORS_KEY, new LinkedList<String>());
		}

		((List<String>) request.args.get(GLOBAL_ERRORS_KEY)).add(errorMessage);
	}

	/**
	 * Store futures into the map for the request.
	 * 
	 * @param request
	 *            The request being processed
	 * @param serviceIdentifier
	 *            The service identifier
	 * @param serviceFuturesMap
	 *            The service futures map
	 */
	@SuppressWarnings("unchecked")
	private static void addServiceFuturesMap(Http.Request request,
			String serviceIdentifier,
			Map<String, ServiceUrlFuture> serviceFuturesMap) {
		if (request.args.get(FUTURES_KEY) == null) {
			request.args.put(FUTURES_KEY,
					new HashMap<String, Map<String, ServiceUrlFuture>>());
		}

		((Map<String, Map<String, ServiceUrlFuture>>) request.args
				.get(FUTURES_KEY)).put(serviceIdentifier, serviceFuturesMap);
	}

	/**
	 * Render the entire feed service response as a JSON with the given status
	 * code.
	 * 
	 * @param request
	 *            The request being processed
	 * @param response
	 *            The response being output
	 * @param httpStatus
	 *            The status code to give
	 */
	private static void renderFeedServiceResponse(Http.Request request,
			Http.Response response, int httpStatus) {
		try {
			response.status = httpStatus;
			String result = buildJSON(
					(Map<String, Map<String, ServiceUrlFuture>>) request.args
							.get(FUTURES_KEY),
					(List<String>) request.args.get(GLOBAL_ERRORS_KEY));
			renderJSON(result);
		} catch (Exception e) {
			// This is a last resort to just at least show something. Should not
			// happen normally.
			FeedserviceLogger.log(Level.ERROR,
					"", request.url, getRequestExecutionTime(),
					"Unexpected error rendering JSON response: " + e.toString());
			error(e);
		}
	}

	/**
	 * Create an error fragment for external service error.
	 * 
	 * @param httpStatus
	 *            The status code to return
	 * @param errorMessage
	 *            The list of error messages
	 * @param responseData
	 *            The HTTP response data from the external service
	 */
	private static String createErrorFragment(int httpStatus,
			String errorMessage, String responseData) {
		JsonObject json = new JsonObject();
		JsonObject error = new JsonObject();

		error.add("httpStatus", new JsonPrimitive(httpStatus));
		if (errorMessage != null) {
			error.add("errorMessage", new JsonPrimitive(errorMessage));
		}

		if (responseData != null) {
			error.add("responseData", new JsonPrimitive(responseData));
		}

		json.add("error", error);

		return json.toString();
	}

	/**
	 * Insert a wrapped data response or regular response based on the
	 * ServiceUrlFuture.
	 * 
	 * @param sb
	 *            The stringbuilder for the response
	 * @param wrapWithData
	 *            Whether to wrap with data
	 * @param response
	 *            The original response
	 * @return wrapped data response
	 */
	private static void insertWrappedDataResponse(StringBuilder sb,
			boolean wrapWithData, String response) {
		if(response == null || response.equals("")) {
			response = EMPTY_RESPONSE;
		}
		if (wrapWithData) {
			sb.append("{\"data\":");
			sb.append(response);
			sb.append("}");
		} else {
			sb.append(response);
		}
	}

	/**
	 * Builds up the JSON string for rendering of the combined results.
	 * 
	 * @param serviceFuturesMaps
	 *            the map of service names to future objects (can be null)
	 * @param errors
	 *            the list of errors if any (can be null or empty)
	 * @return a JSON string for all the results obtained
	 * 
	 * @throws ExternalServiceException
	 *             some unexpected error condition from external services which
	 *             we defensively catch for better debuggability
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	private static String buildJSON(
			Map<String, Map<String, ServiceUrlFuture>> serviceFuturesMaps,
			List<String> errors) throws ExternalServiceException,
			InterruptedException, ExecutionException {

		StringBuilder result = new StringBuilder("{"); // Starting brace

		// Store the global errors as an array at the top level.
		if (errors != null && errors.size() > 0) {
			JsonArray json = new JsonArray();
			for (String error : errors) {
				json.add(new JsonPrimitive(error));
			}
			result.append("\"globalErrors\":");
			result.append(json.toString());
			result.append(",");
		}

		// Not using JSON libraries here to preserve the JSON payloads from the
		// external services
		// without doing much serialization/deserialization.
		if (serviceFuturesMaps != null) {
			boolean firstService = true;
			for (String serviceIdentifier : serviceFuturesMaps.keySet()) {
				if (firstService) {
					firstService = false;
				} else {
					result.append(", ");
				}

				result.append("\"");
				result.append(serviceIdentifier);
				result.append("\":{"); // Start for each service

				Map<String, ServiceUrlFuture> futures = (Map<String, ServiceUrlFuture>) serviceFuturesMaps
						.get(serviceIdentifier);
				boolean firstFuture = true;
				for (Map.Entry<String, ServiceUrlFuture> futureEntry : futures
						.entrySet()) {
					String futureName = futureEntry.getKey();
					if (firstFuture) {
						firstFuture = false;
					} else {
						result.append(", ");
					}

					result.append("\"");
					result.append(futureName);
					result.append("\":");

					ServiceUrlFuture serviceUrlFuture = futureEntry.getValue();
					Future<WS.HttpResponse> future = serviceUrlFuture
							.getFuture();
					String requestIdentifier = serviceIdentifier + " "
						+ futureName + " " + serviceUrlFuture.getUrl();
					try {
						if (future == null) {
							throw new ExternalServiceException(
									"Future response should not be null for future name: "
											+ requestIdentifier);
						} else {
							WS.HttpResponse futureResponse = future.get();
							if (futureResponse == null) {
								throw new ExternalServiceException(
										"External service URL could not be connected to: "
												+ requestIdentifier);
							} else if (futureResponse.getStatus() != HttpStatus.SC_OK) {
								throw new ExternalServiceException(
										"External service URL return non-success: "
												+ requestIdentifier,
										futureResponse.getStatus(),
										futureResponse.getString(),
										futureResponse.getHeaders());
							} else {
								// We check to see if this is a data response
								// that needs our own wrapping, and do so.
								insertWrappedDataResponse(result,
										serviceUrlFuture.getWrapWithData(),
										futureResponse.getString());
							}
						}
					} catch (ExternalServiceException e) {
						FeedserviceLogger.log(Level.ERROR,
								"", "", getRequestExecutionTime(),
								"External service error: " + e.toString());
						result.append(createErrorFragment(e.getHttpStatus(),
								e.getMessage(), e.getResponseData()));
					} catch (Error e) {
						FeedserviceLogger.log(Level.ERROR,
								"", "", getRequestExecutionTime(),
								"Unexpected error: " + e.toString());
						result.append(createErrorFragment(
								UNEXPECTED_ERROR_STATUS_CODE, e.getMessage(),
								null));
					}
				}

				result.append("}"); // End for each service
			}
		}

		result.append("}"); // Ending brace
		return result.toString();
	}

	@play.mvc.Util
	public static void performOauthAction(OauthAction action) {
		final long requestStartTime = System.currentTimeMillis();
		request.args.put(REQUEST_START_TIME_KEY, requestStartTime);
		
		String serviceParam = params.get(SERVICE_PARAM);
		// Fail fast if invalid number of requested services
		if (serviceParam == null || serviceParam.trim().length() == 0) {
			addGlobalError(request, "Invalid services parameter string");
			renderFeedServiceResponse(request, response,
					HttpStatus.SC_BAD_REQUEST);
		}

		String serviceIdentifier = serviceParam;
		BaseOAuthHandler oauthHandler = OAUTH_HANDLERS.get(serviceIdentifier
				.toLowerCase());
		try {
			Object response = oauthHandler.performAction(action, params);
			Map jsonMap = oauthHandler.renderOauthResponse(action, response);
			renderJSON(jsonMap);
		} catch (IOException e) {
		}

	}

	@play.mvc.Util
	public static void checkIfIsAlive() {

		HttpResponse res1 = WS.url("https://api.linkedin.com/v1/search.json")
				.get();
		HttpResponse res2 = WS
				.url("https://api.twitter.com/1.1/users/show.json?screen_name=benioff")
				.get();
		HttpResponse res3 = WS.url("https://graph.facebook.com/benioff").get();

		renderText(res1.getString() + "\n" + res2.getString() + "\n"
				+ res3.getString());
	}

	@play.mvc.Util
	public static void getConnectionCount() {
		BufferedReader stdInput = null;
		BufferedReader stdError = null;
		int connectionCount = -1;
		try {
			// use "lsof -l" to get the number of open connections
			Process p = Runtime.getRuntime().exec("netstat -vatn | wc -l");

			stdInput = new BufferedReader(new InputStreamReader(
					p.getInputStream()));

			stdError = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));
		} catch (IOException e) {
			FeedserviceLogger.log(Level.ERROR,
					"", "", 0,
					e.toString());
		}

		try {
			connectionCount = stdInput.read();
		} catch (IOException e) {
			FeedserviceLogger.log(Level.ERROR,
					"", "", 0,
					e.toString());
		}
		renderText(connectionCount);
	}
    
    @play.mvc.Util
    public static void getPing(){
    	renderText("ALIVE\n");
    }
    
    @play.mvc.Util
	public static void checkExternalAccess() {    
    	try{
    		HttpResponse resp = WS.url("http://www.twitter.com/help/test.json").timeout("2s").get();
    		if(resp.success()) { 
    			//status code 20x
    			renderText("ALIVE\n");
    		}
    		else{
    			//non-20x status code which means response was received but external url needs to be checked
    			renderText("NON-20x ALIVE\n");
    		}
    	}catch(Exception ex){
    		FeedserviceLogger.log(Level.ERROR,
					"", "/check", 0,
					ex.toString());
    		renderText("DOWN\n");
    	}
	}
    
    @play.mvc.Util
    protected static long getRequestExecutionTime(){
    	Long requestStartTime = (Long)request.args.get(REQUEST_START_TIME_KEY);
    	
    	if(requestStartTime != null) {
    		return (System.currentTimeMillis() - requestStartTime); 
    	}else{
    		return 0;
    	}
    	
    }
}
