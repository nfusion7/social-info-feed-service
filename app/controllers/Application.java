package controllers;

import java.io.IOException;

import util.Action;
import util.OauthAction;

public class Application extends Base {
    /**
     * Renders the combined results of JSON payloads from each requested service.
     * Uses a request parameter to determine which services to look up.
     * The services param is expected to be the form of a lowercase, comma-delimited string
     * of service identifiers.
     */
	public static void profile() {
		performAction(Action.PROFILE);
    }
	
    /**
     * Searches through the services for matching handles based
     * on a query string (which is usually the users' name).
     */
    public static void search() {
    	performAction(Action.SEARCH);
    }
    
    /**
     * Performs an api query to the specified service. Returns the results as received.
     */
    public static void query() {
    	performAction(Action.QUERY);
    }
    
    /**
     * Get the url for OAuth signin 
     */
    public static void getOauthLink() throws IOException {
    	performOauthAction(OauthAction.OAUTH_LINK);
    }
    
    /**
     * Get the OAuth access token
     */
    public static void getAccessToken() throws IOException {
    	performOauthAction(OauthAction.ACCESS_TOKEN);
    }
    
    
    /**
     * Performs a health check to see if the feeds service can talk to the endpoints
     */
    public static void isAlive() {
    	checkIfIsAlive();
    }
    
    /**
     * Displays the number of open ports and TCP connections on this server.
     */
    public static void displayConnectionCount() {
    	getConnectionCount();
    }

    /**
     * Hits a static url on the web and check end-end connectivity.
     * Similar to isAlive but only hits a static twitter url which does not eat up the api quota. 
     */
    public static void check() {
    	checkExternalAccess();
    }
}