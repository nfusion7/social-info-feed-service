import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.mockito.Mockito;

import org.junit.*;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import play.Invoker.Suspend;
import play.test.*;
import play.libs.WS;
import play.mvc.*;
import play.mvc.Http.*;
import play.mvc.Scope.Params;
import services.*;
import testing.TestRequestResponse;
import testing.TestableFutureFactory;

public class TwitterServiceTest extends FeedServiceTest {
    private static final String MISSING_PARAMETERS = "Missing parameters for requested service: tw";
    private static final String TWITTER_API_BASE_URL = "https://api.twitter.com/1.1";
    /**
     * Returns the service being tested.
     *
     * @return Service class being tested
     */
    protected Service getServiceBeingTested() {
        return new TwitterService();
    }

    @Test
    public void testThatCallTwitterApiRequiresScreenName() {
    	serviceGETCall("/profile.json?services=tw",
    			400, MISSING_PARAMETERS);
    }

    @Test
    public void testThatCallTwitterApiRequiresOAuthToken () {
    	serviceGETCall("/profile.json?services=tw&twUserIdentifier=ilyasu",
    			400, MISSING_PARAMETERS);
    }

    @Test
    public void testThatCallTwitterApiRequiresOAuthSecret() {
    	serviceGETCall("/profile.json?services=tw&twUserIdentifier=ilyasu&twOauthToken=foo",
    			400, MISSING_PARAMETERS);
    }

    @Test
    public void testThatCallTwitterApiUserIdentifierEmpty() {
    	serviceGETCall("/profile.json?services=tw&twUserIdentifier=&twOauthToken=foo&twOauthSecret=aoeu",
    			400, MISSING_PARAMETERS);
    }

    @Test
    public void testThatCallTwitterApiOAuthSecretEmpty() {
    	serviceGETCall("/profile.json?services=tw&twUserIdentifier=ilyasu&twOauthToken=foo&twOauthSecret=",
    			400, MISSING_PARAMETERS);
    }

    @Test
    public void testThatCallTwitterApiParamsInCaps() {
    	serviceGETCall("/profile.json?SERVICES=TW&TWUSERIDENTIFIER=ilyasu&TWOAUTHTOKEN=foo&TWOAUTHSECRET=aoeu",
    			400, "Invalid services parameter string");
    }

    @Test
    public void testThatCallTwitterApiReturnsJSONCorrectlyIfThereAreNoErrors() {
    	TestRequestResponse[] mockRequestResponses = {
                new TestRequestResponse(
                        "userInfo",
                        TWITTER_API_BASE_URL+"/users/show.json?screen_name=ilyasu",
                        "{ \"fakeUserInfo\": 0 }"),
                new TestRequestResponse(
                        "userTimeline",
                        TWITTER_API_BASE_URL+"/statuses/user_timeline.json?count=20&trim_user=1&screen_name=ilyasu",
                        "{ \"fakeUserTimeline\": 0 }",
                        true),
        };

        setupMockResponsesForTwitter(mockRequestResponses);

        String serviceResponse = getResponse("http://localhost:9000/profile.json?services=tw&twUserIdentifier=ilyasu&twOauthSecret=aoeu&twOauthToken=foo");
        assertResponse(serviceResponse, mockRequestResponses);
        teardownTestEnvironment();
    }

    @Test
    public void testThatCallTwitterApiDealsWithLocalErrorsCorrectly() {
        // This test ensures local errors are present, and the correct successful JSON is returned for 1 service.
    	TestRequestResponse[] mockRequestResponses = {
                new TestRequestResponse(
                        "userInfo",
                        TWITTER_API_BASE_URL+"/users/show.json?screen_name=ilyasu",
                        "fake error string",
                        400),
                new TestRequestResponse(
                        "userTimeline",
                        TWITTER_API_BASE_URL+"/statuses/user_timeline.json?count=20&screen_name=ilyasu",
                        "another fake error string",
                        500),
        };

        setupMockResponsesForTwitter(mockRequestResponses);

        String serviceResponse = getResponse("http://localhost:9000/profile.json?services=tw&twUserIdentifier=ilyasu&twOauthSecret=aoeu&twOauthToken=foo");
        assertResponse(serviceResponse, mockRequestResponses);
        teardownTestEnvironment();
    }

    private void setupMockResponsesForTwitter(TestRequestResponse[] mockRequestResponses) {
        setupMockResponses(mockRequestResponses, true, new HashMap<String,String>());
    }

    @Test
    public void testTwitterServiceGetUrlsIsValid() {
    	Params serviceParams = new Params();

    	serviceParams.put("twUserIdentifier", "duplicateParam");  // should be ignored because is overwritten later
    	serviceParams.put("twUserIdentifier", "tweeter");
    	serviceParams.put("twOauthToken", "bird");
    	serviceParams.put("twOauthSecret", "chirp");
    	serviceParams.put("liUserIdentifier", "IvOeMeDiv");  // should not be included in twitter request
    	Map<String, ServiceUrlFuture> actualUrls = getServiceUrls("tw", serviceParams);

    	String expectedUserInfoRequest = TWITTER_API_BASE_URL+"/users/show.json?screen_name=tweeter";
    	String expectedUserTimelineRequest = TWITTER_API_BASE_URL+"/statuses/user_timeline.json?count=20&screen_name=tweeter";

    	assertEquals("UserInfo request for twitter incorrect", expectedUserInfoRequest, actualUrls.get("userInfo").getUrl());
        assertEquals("UserInfo request for twitter incorrect", false, actualUrls.get("userInfo").getWrapWithData());
    	assertEquals("UserTimeline request for twitter incorrect", expectedUserTimelineRequest, actualUrls.get("userTimeline").getUrl());
        assertEquals("UserTimeline request for twitter incorrect", true, actualUrls.get("userTimeline").getWrapWithData());
    }

    @Test
	public void testTwitterSearch() {
		TestRequestResponse[] mockRequestResponses = {
				new TestRequestResponse(
						"userSearch",
						TWITTER_API_BASE_URL+"/users/search.json?per_page=20&q=Mark",
						"{ \"fakeUserSearch\": 0 }",
                        true)
		};

		setupMockResponsesForTwitter(mockRequestResponses);

		String serviceResponse = getResponse("http://localhost:9000/search.json?services=tw&twOauthSecret=aoeu&twOauthToken=foo&searchTerm=Mark");
		assertResponse(serviceResponse, mockRequestResponses);
		teardownTestEnvironment();
	}

	@Test
	public void testTwitterSearchRequiresOuthToken() {
		serviceGETCall("/search.json?services=tw&twOauthSecret=aeio&q=benioff", 400,
		"Missing parameters");
	}

	@Test
	public void testTwitterSearchRequiresOuthSeceret() {
		serviceGETCall("/search.json?services=tw&twOauthToken=1234&q=benioff", 400,
		"Missing parameters");
	}

	@Test
	public void testTwitterSearchRequiresQueryString() {
		serviceGETCall("/search.json?services=tw&twOauthToken=1234&twOauthSecret=aeiou&", 400,
		"Missing parameters");
	}

	@Test
	public void testTwitterSearchEmptyQueryString() {
		serviceGETCall("/search.json?services=tw&twOauthToken=1234&twOauthSecret=aeio&q=", 400,
		"Missing parameters");
	}
	
	@Test
	public void testTwitterQuery() {
		TestRequestResponse[] mockRequestResponses = {
				new TestRequestResponse(
						"apiQueryResult",
						TWITTER_API_BASE_URL+"/somequeryurl",
						"{ \"fakeUserQuery\": 0 }")
		};

		setupMockResponsesForTwitter(mockRequestResponses);

		String serviceResponse = getResponse("http://localhost:9000/query.json?services=tw&twOauthToken=1234&twOauthSecret=aeioo&apiQueryUrl="+TWITTER_API_BASE_URL+"/somequeryurl");
		assertResponse(serviceResponse, mockRequestResponses);
		teardownTestEnvironment();
	}
	
	@Test
	public void testTwitterQueryRequiresQueryUrl() {
		serviceGETCall("/search.json?services=tw&twOauthToken=1234&twOauthSecret=aeio", 400,
		"Missing parameters");
	}
	
	@Test
	public void testTwitterQueryRequiresOauthToken() {
		serviceGETCall("/search.json?services=tw&twOauthSecret=aeio&apiQueryUrl="+TWITTER_API_BASE_URL+"/somequeryurl", 400,
		"Missing parameters");
	}
	
	@Test
	public void testTwitterQueryRequiresOauthSecret() {
		serviceGETCall("/search.json?services=tw&twOauthToken=1234&apiQueryUrl="+TWITTER_API_BASE_URL+"/somequeryurl", 400,
		"Missing parameters");
	}

	@Test
	public void testTwitterQueryEmptyQueryString() {
		serviceGETCall("/search.json?services=tw&twOauthToken=1234&twOauthSecret=aeio&apiQueryUrl=", 400,
		"Missing parameters");
	}
	
	@Test
	public void testTwitterQueryInvalidEndpoint() {
		serviceGETCall("/search.json?services=tw&twOauthToken=1234&twOauthSecret=aeio&apiQueryUrl=https://graph.facebook.com/somequeryurl", 400,
		"Missing parameters");
	}
}
