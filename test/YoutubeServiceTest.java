import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.mockito.Mockito;

import org.junit.*;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import play.Invoker.Suspend;
import play.test.*;
import play.libs.WS;
import play.mvc.*;
import play.mvc.Http.*;
import play.mvc.Scope.Params;

import services.*;
import testing.TestRequestResponse;
import util.Action;

import java.util.HashMap;
import java.util.Map;

public class YoutubeServiceTest extends FeedServiceTest {
    private static final String MISSING_PARAMETERS = "Missing parameters for requested service: yt";
    private static final String KEY = "AI39si4T3CHyt0VfkitJ8CoypvCCZNhoM0b0N2ApUMei46pkrqHIDBkA9b0O9c_HcMWojxZG2iuKWluVeMRXJjaEVfRAz5-mlw";
    private static final String YOUTUBE_SEARCH_URL = "https://gdata.youtube.com/feeds/api/videos?alt=json&max-results=25&v=2&start-index=1&key="+KEY+"&";

	/**
	 * Returns the service being tested.
	 *
	 * @return Service class being tested
	 */
	protected Service getServiceBeingTested() {
		return new YouTubeService();
	}

	//@Test
	//public void testThatCallYoutubeDoesNotSupportProfileCall() {
	//	serviceGETCall("/profile.json?services=yt&searchTerm=mark&orderBy=published", 400,
	//			"ActionName PROFILE not supported for service: yt");
	//}

	@Test
	public void testThatCallYoutubeSearchRequiresSearchParams() {
		serviceGETCall("/search.json?services=yt", 400, MISSING_PARAMETERS);
	}

	@Test
	public void testYoutubeSearchRequiresQueryString() {
		serviceGETCall("/search.json?services=yt&orderBy=published", 400,
		"Missing parameters");
	}

	@Test
	public void testYoutubeSearchEmptyQueryString() {
		serviceGETCall("/search.json?services=yt&searchTerm=&orderBy=published", 400,
		"Missing parameters");
	}
	
	@Test
	public void testThatCallYoutubeSearchParamsInCaps() {
		serviceGETCall(
				"/search.json?SERVICES=YT&SEARCHTERM=mark&ORDERBY=published",
				400, "Invalid services parameter string");
	}
	
	@Test
	public void testThatCallYoutubeApiReturnsJSONCorrectlyIfThereAreNoErrors() {
		TestRequestResponse[] mockRequestResponses = {
				new TestRequestResponse(
						"userSearch",
						YOUTUBE_SEARCH_URL + "q=Mark&orderby=published",
						"{ \"fakeUserSearch\": 0 }")
		};

		setupMockResponsesForYoutube(mockRequestResponses);

		String serviceResponse = getResponse("http://localhost:9000/search.json?services=yt&searchTerm=Mark&orderBy=published");
		assertResponse(serviceResponse, mockRequestResponses);
		teardownTestEnvironment();
	}

    @Test
    public void testThatCallYoutubeApiDealsWithLocalErrorsCorrectly() {
        // This test ensures local errors are present, and the correct successful JSON is returned for 1 service.
    	TestRequestResponse[] mockRequestResponses = {
                new TestRequestResponse(
                		"userSearch",
                		YOUTUBE_SEARCH_URL + "q=Mark&orderby=published",
                        "fake error string",
                        500),
        };

        setupMockResponsesForYoutube(mockRequestResponses);

        String serviceResponse = getResponse("http://localhost:9000/search.json?services=yt&searchTerm=Mark");
        assertResponse(serviceResponse, mockRequestResponses);
        teardownTestEnvironment();
    }

	private void setupMockResponsesForYoutube(TestRequestResponse[] mockRequestResponses) {
		setupMockResponses(mockRequestResponses, false,	new HashMap<String, String>());
	}

	@Test
	public void testYoutubeServiceGetUrlsIsValid() {
		Params serviceParams = new Params();

		serviceParams.put("twUserIdentifier", "tweeter");  // should not be included in Youtube request
		serviceParams.put("searchTerm", "duplicateParam"); // should be ignored because is overwritten later
		serviceParams.put("searchTerm", "pirate");
		serviceParams.put("orderBy", "myOrder");
		Map<String, ServiceUrlFuture> actualUrls = getServiceUrls("yt", serviceParams, Action.SEARCH);

		String expectedUserInfoRequest = YOUTUBE_SEARCH_URL + "q=pirate&orderby=myOrder";

		assertEquals("UserInfo request for Youtube incorrect", expectedUserInfoRequest, actualUrls.get("userSearch").getUrl());
        assertEquals("UserInfo request for Youtube incorrect", false, actualUrls.get("userSearch").getWrapWithData());
	}
	
}
