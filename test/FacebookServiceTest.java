import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.mockito.Mockito;

import org.junit.*;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import play.Invoker.Suspend;
import play.test.*;
import play.libs.WS;
import play.mvc.*;
import play.mvc.Http.*;
import play.mvc.Scope.Params;

import services.*;
import testing.TestRequestResponse;

import java.util.HashMap;
import java.util.Map;

public class FacebookServiceTest extends FeedServiceTest {
    private static final String MISSING_PARAMETERS = "Missing parameters for requested service: fb";

	/**
	 * Returns the service being tested.
	 *
	 * @return Service class being tested
	 */
	protected Service getServiceBeingTested() {
		return new FacebookService();
	}

	@Test
	public void testThatCallFacebookApiRequiresScreenName() {
		serviceGETCall("/profile.json?services=fb", 400, MISSING_PARAMETERS);
	}

	@Test
	public void testThatCallFacebookApiRequiresAccessToken() {
		serviceGETCall("/profile.json?services=fb&fbUserIdentifier=evanbeard", 400, MISSING_PARAMETERS);
	}


	@Test
	public void testThatCallFacebookApiUserIdentifierEmpty() {
		serviceGETCall("/profile.json?services=fb&fbUserIdentifier=&fbAccessToken=1234", 400,
		MISSING_PARAMETERS);
	}


	@Test
	public void testThatCallFacebookApiAccessTokenEmpty() {
		serviceGETCall("/profile.json?services=fb&fbUserIdentifier=evanbeard&fbAccessToken=",
				400, MISSING_PARAMETERS);
	}


	@Test
	public void testThatCallFacebookApiParamsInCaps() {
		serviceGETCall(
				"/profile.json?SERVICES=FB&FBUSERIDENTIFIER=evanbeard&FBACCESSTOKENTOKEN=1234",
				400, "Invalid services parameter string");
	}

	@Test
	public void testThatCallFacebookApiReturnsJSONCorrectlyIfThereAreNoErrors() {
		TestRequestResponse[] mockRequestResponses = {
				new TestRequestResponse(
						"userInfo",
						"https://graph.facebook.com/evanbeard?access_token=1234",
						"{ \"fakeUserInfo\": 0 }"),
				new TestRequestResponse(
						"userTimeline",
						"https://graph.facebook.com/evanbeard/posts?access_token=1234",
						"{ \"fakeUserTimeline\": 0 }"),
		};

		setupMockResponsesForFacebook(mockRequestResponses);

		String serviceResponse = getResponse("http://localhost:9000/profile.json?services=fb&fbUserIdentifier=evanbeard&fbOauthToken=1234");
		assertResponse(serviceResponse, mockRequestResponses);
		teardownTestEnvironment();
	}

    @Test
    public void testThatCallFacebookApiDealsWithLocalErrorsCorrectly() {
        // This test ensures local errors are present, and the correct successful JSON is returned for 1 service.
    	TestRequestResponse[] mockRequestResponses = {
                new TestRequestResponse(
                        "userInfo",
                        "https://graph.facebook.com/evanbeard?return_ssl_resources=1&access_token=1234",
                        "fake error string",
                        400),
                new TestRequestResponse(
                        "userTimeline",
                        "https://graph.facebook.com/evanbeard/posts?return_ssl_resources=1&access_token=1234",
                        "another fake error string",
                        500),
        };

        setupMockResponsesForFacebook(mockRequestResponses);

        String serviceResponse = getResponse("http://localhost:9000/profile.json?services=fb&fbUserIdentifier=evanbeard&fbOauthToken=1234");
        assertResponse(serviceResponse, mockRequestResponses);
        teardownTestEnvironment();
    }

	private void setupMockResponsesForFacebook(TestRequestResponse[] mockRequestResponses) {
		setupMockResponses(mockRequestResponses, false,	new HashMap<String, String>());
	}

	@Test
	public void testFacebookServiceGetUrlsIsValid() {
		Params serviceParams = new Params();

		serviceParams.put("twUserIdentifier", "tweeter"); // should not be included in facebook request
		serviceParams.put("fbUserIdentifier", "duplicateParam"); // should be ignored because is overwritten later
		serviceParams.put("fbUserIdentifier", "pirate");
		serviceParams.put("fbOauthToken", "privacy");
		Map<String, ServiceUrlFuture> actualUrls = getServiceUrls("fb", serviceParams);

		String expectedUserInfoRequest = "https://graph.facebook.com/pirate?return_ssl_resources=1&access_token=privacy";
		String expectedUserTimelineRequest = "https://graph.facebook.com/pirate/posts?return_ssl_resources=1&access_token=privacy";

		assertEquals("UserInfo request for Facebook incorrect", expectedUserInfoRequest, actualUrls.get("userInfo").getUrl());
        assertEquals("UserInfo request for Facebook incorrect", false, actualUrls.get("userInfo").getWrapWithData());
		assertEquals("UserTimeline request for Facebook incorrect", expectedUserTimelineRequest, actualUrls.get("userTimeline").getUrl());
        assertEquals("UserTimeline request for Facebook incorrect", false, actualUrls.get("userTimeline").getWrapWithData());
	}

	@Test
	public void testFacebookSearch() {
		TestRequestResponse[] mockRequestResponses = {
				new TestRequestResponse(
						"userSearch",
						"https://graph.facebook.com/search?type=user&fields=id,name,picture,education,work,location&access_token=1234&q=Mark",
						"{ \"fakeUserSearch\": 0 }")
		};

		setupMockResponsesForFacebook(mockRequestResponses);

		String serviceResponse = getResponse("http://localhost:9000/search.json?services=fb&fbOauthToken=1234&searchTerm=Mark");
		assertResponse(serviceResponse, mockRequestResponses);
		teardownTestEnvironment();
	}

	@Test
	public void testFacebookSearchRequiresOuthToken() {
		serviceGETCall("/search.json?services=fb&q=benioff", 400,
		"Missing parameters");
	}

	@Test
	public void testFacebookSearchRequiresQueryString() {
		serviceGETCall("/search.json?services=fb&fbAccessToken=1234", 400,
		"Missing parameters");
	}

	@Test
	public void testFacebookSearchEmptyQueryString() {
		serviceGETCall("/search.json?services=fb&fbAccessToken=1234&q=", 400,
		"Missing parameters");
	}
	
	@Test
	public void testFacebookQuery() {
		TestRequestResponse[] mockRequestResponses = {
				new TestRequestResponse(
						"apiQueryResult",
						"https://graph.facebook.com/somequeryurl&access_token=1234",
						"{ \"fakeUserQuery\": 0 }")
		};

		setupMockResponsesForFacebook(mockRequestResponses);

		String serviceResponse = getResponse("http://localhost:9000/query.json?services=fb&fbOauthToken=1234&apiQueryUrl=https://graph.facebook.com/somequeryurl");
		assertResponse(serviceResponse, mockRequestResponses);
		teardownTestEnvironment();
	}
	
	@Test
	public void testFacebookQueryRequiresOuthToken() {
		serviceGETCall("/search.json?services=fb&apiQueryUrl=https://graph.facebook.com/somequeryurl", 400,
		"Missing parameters");
	}

	@Test
	public void testFacebookQueryRequiresQueryUrl() {
		serviceGETCall("/search.json?services=fb&fbAccessToken=1234", 400,
		"Missing parameters");
	}

	@Test
	public void testFacebookQueryEmptyQueryString() {
		serviceGETCall("/search.json?services=fb&fbAccessToken=1234&apiQueryUrl=", 400,
		"Missing parameters");
	}
	
	@Test
	public void testFacebookQueryInvalidEndpoint() {
		serviceGETCall("/search.json?services=fb&fbAccessToken=1234&apiQueryUrl=https://api.twitter.com/1.1/somequeryurl", 400,
		"Missing parameters");
	}
}