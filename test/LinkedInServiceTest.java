import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.mockito.Mockito;

import org.junit.*;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import play.Invoker.Suspend;
import play.test.*;
import play.libs.WS;
import play.mvc.*;
import play.mvc.Http.*;
import play.mvc.Scope.Params;

import services.*;
import testing.TestRequestResponse;

import com.google.common.collect.ImmutableMap;

public class LinkedInServiceTest extends FeedServiceTest {
    private static final String MISSING_PARAMETERS = "Missing parameters for requested service: li";

    /**
     * Returns the service being tested.
     *
     * @return Service class being tested
     */
    protected Service getServiceBeingTested() {
        return new LinkedInService();
    }

    @Test
    public void testThatCallLinkedInApiRequiresScreenName() {
    	serviceGETCall("/profile.json?services=li",
    			400, MISSING_PARAMETERS);
    }

    @Test
    public void testThatCallLinkedInApiRequiresOAuthToken () {
    	serviceGETCall("/profile.json?services=li&liUserIdentifier=IvOeMeDivK",
                400, MISSING_PARAMETERS);
    }

    @Test
    public void testThatCallLinkedInApiRequiresOAuthSecret() {
    	serviceGETCall("/profile.json?services=li&liUserIdentifier=IvOeMeDivK&tOauthToken=foo",
                400, MISSING_PARAMETERS);
    }

    @Test
    public void testThatCallLinkedInApiUserIdentifierEmpty() {
    	serviceGETCall("/profile.json?services=li&liUserIdentifier=&liOauthToken=foo&liOauthSecret=bar",
                400, MISSING_PARAMETERS);
    }

    @Test
    public void testThatCallLinkedInApiOAuthSecretEmpty() {
    	serviceGETCall("/profile.json?services=li&liUserIdentifier=IvOeMeDivK&liOauthToken=foo&liOauthSecret=",
                400, MISSING_PARAMETERS);
    }


    @Test
    public void testThatCallLinkedInApiParamsInCaps() {
    	serviceGETCall("/profile.json?SERVICES=LI&LIUSERIDENTIFIER=IvOeMeDivK&LIOAUTHTOKEN=foo&LIOAUTHSECRET=bar",
    			400, "Invalid services parameter string");
    }

    @Test
    public void testThatCallLinkedInApiReturnsJSONCorrectlyIfThereAreNoErrors() {
        TestRequestResponse[] mockRequestResponses = {
                new TestRequestResponse(
                        "userInfo",
                        "https://api.linkedin.com/v1/people/id=IvOeMeDivK" +
                            ":(id,first-name,last-name,public-profile-url,location:(name),headline,picture-url," +
                            "educations,positions,relation-to-viewer:(num-related-connections,related-connections," +
                            "distance),distance,site-standard-profile-request:(url))",
                        "{ \"fakeUserInfo\": 0 }"),
        };

        setupMockResponsesForLinkedIn(mockRequestResponses);

        String serviceResponse = getResponse("http://localhost:9000/profile.json?services=li&liUserIdentifier=IvOeMeDivK&liOauthToken=foo&liOauthSecret=bar");
        assertResponse(serviceResponse, mockRequestResponses);
        teardownTestEnvironment();
    }

    @Test
    public void testThatCallLinkedInApiDealsWithLocalErrorsCorrectly() {
        // This test ensures local errors are present, and the correct successful JSON is returned for 1 service.
    	TestRequestResponse[] mockRequestResponses = {
                new TestRequestResponse(
                        "userInfo",
                        "https://api.linkedin.com/v1/people/id=IvOeMeDivK" +
                            ":(id,first-name,last-name,public-profile-url,location:(name),headline,picture-url," +
                            "educations,positions,relation-to-viewer:(num-related-connections,related-connections," +
                            "distance),distance,site-standard-profile-request:(url))",
                        "fake error string",
                        400),
        };

        setupMockResponsesForLinkedIn(mockRequestResponses);

        String serviceResponse = getResponse("http://localhost:9000/profile.json?services=li&liUserIdentifier=IvOeMeDivK&liOauthToken=foo&liOauthSecret=bar");
        assertResponse(serviceResponse, mockRequestResponses);
        teardownTestEnvironment();
    }

    private void setupMockResponsesForLinkedIn(TestRequestResponse[] mockRequestResponses) {
    	setupMockResponses(mockRequestResponses, true, ImmutableMap.of((String)"x-li-format", (String)"json"));
    }

    @Test
    public void testLinkedInServiceGetUrlsIsValid() {
    	Params serviceParams = new Params();

    	serviceParams.put("liUserIdentifier", "duplicateParam");  // should be ignored because is overwritten later
    	serviceParams.put("liUserIdentifier", "bigboss");
    	serviceParams.put("liOauthToken", "token");
    	serviceParams.put("liOauthSecret", "shhh");
    	serviceParams.put("fbUserIdentifier", "ziggy");  // should not be included in linkedin request
    	Map<String, ServiceUrlFuture> actualUrls = getServiceUrls("li", serviceParams);

    	String expectedUserInfoRequest = "https://api.linkedin.com/v1/people/id=bigboss" +
        	":(id,first-name,last-name,public-profile-url,location:(name),headline,picture-url," +
        	"educations,positions,relation-to-viewer:(num-related-connections,related-connections," +
        	"distance),distance,site-standard-profile-request:(url))";

    	assertEquals("UserInfo request for LinkedIn incorrect", expectedUserInfoRequest, actualUrls.get("userInfo").getUrl());
        assertEquals("UserInfo request for LinkedIn incorrect", false, actualUrls.get("userInfo").getWrapWithData());
    }

	@Test
	public void testLinkedInSearch() {
		TestRequestResponse[] mockRequestResponses = {
				new TestRequestResponse(
						"userSearch",
						"https://api.linkedin.com/v1/people-search:(people:(id,first-name,last-name," +
						"headline,location:(name),educations,positions,picture-url),num-results)?sort=relevance&keywords=Mark+Johnson",
						"{ \"fakeUserSearch\": 0 }")
		};

		setupMockResponsesForLinkedIn(mockRequestResponses);

		String serviceResponse = getResponse("http://localhost:9000/search.json?services=li&liOauthToken=1234" +
				"&liOauthSecret=345&searchTerm=Mark%20Johnson");
		assertResponse(serviceResponse, mockRequestResponses);
		teardownTestEnvironment();
	}

	@Test
	public void testLinkedInSearchRequiresOuthToken() {
		serviceGETCall("/search.json?services=li&liOauthSecret=aeio&q=benioff", 400,
		"Missing parameters");
	}

	@Test
	public void testLinkedInSearchRequiresOuthSeceret() {
		serviceGETCall("/search.json?services=li&liOauthToken=1234&q=benioff", 400,
		"Missing parameters");
	}

	@Test
	public void testLinkedInSearchRequiresQueryString() {
		serviceGETCall("/search.json?services=li&liOauthToken=1234&liOauthSecret=aeiou&", 400,
		"Missing parameters");
	}

	@Test
	public void testLinkedInSearchEmptyQueryString() {
		serviceGETCall("/search.json?services=li&liOauthToken=1234&liOauthSecret=aeio&q=", 400,
		"Missing parameters");
	}
		
	@Test
	public void testLinkedInQuery() {
		TestRequestResponse[] mockRequestResponses = {
				new TestRequestResponse(
						"apiQueryResult",
						"https://api.linkedin.com/v1/somequeryurl&access_token=1234&liOauthSecret=aeio",
						"{ \"fakeUserQuery\": 0 }")
		};

		setupMockResponsesForLinkedIn(mockRequestResponses);

		String serviceResponse = getResponse("http://localhost:9000/query.json?services=li&liOauthToken=1234&liOauthSecret=aeio&apiQueryUrl=https://api.linkedin.com/v1/somequeryurl");
		assertResponse(serviceResponse, mockRequestResponses);
		teardownTestEnvironment();
	}
	
	@Test
	public void testLinkedInQueryRequiresOuthToken() {
		serviceGETCall("/search.json?services=li&liOauthSecret=aeio&apiQueryUrl=https://api.linkedin.com/v1/somequeryurl", 400,
		"Missing parameters");
	}

	@Test
	public void testLinkedInQueryRequiresOuthSecret() {
		serviceGETCall("/search.json?services=li&liAccessToken=1234&apiQueryUrl=https://api.linkedin.com/v1/somequeryurl", 400,
		"Missing parameters");
	}
	
	@Test
	public void testLinkedInQueryRequiresQueryUrl() {
		serviceGETCall("/search.json?services=li&liAccessToken=1234&liOauthSecret=aeio", 400,
		"Missing parameters");
	}

	@Test
	public void testLinkedInQueryEmptyQueryString() {
		serviceGETCall("/search.json?services=li&liAccessToken=1234&liOauthSecret=aeio&apiQueryUrl=", 400,
		"Missing parameters");
	}
	
	@Test
	public void testLinkedInQueryInvalidEndpoint() {
		serviceGETCall("/search.json?services=li&liAccessToken=1234&liOauthSecret=aeio&apiQueryUrl=https://api.twitter.com/1.1/somequeryurl", 400,
		"Missing parameters");
	}
}
