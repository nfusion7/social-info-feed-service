import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

import org.apache.http.HttpStatus;
import org.junit.Test;
import org.mockito.Mockito;

import play.libs.WS;
import play.mvc.Http.Response;
import play.mvc.Scope.Params;
import play.test.FunctionalTest;
import services.Service;
import services.ServiceUrlFuture;
import testing.TestRequestResponse;
import testing.TestableFutureFactory;
import util.Action;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;

import controllers.Application;
import controllers.FutureFactory;

/**
 * Tests for the entire FeedService controller.
 */
public class FeedServiceTest extends FunctionalTest {
    /**
     * Returns the service being tested.
     * @return Service being tested
     */
    protected Service getServiceBeingTested() {
        return null;
    }

    /**
     * Helper method for all derivative service tests to get the response
     * of hitting a URL.
     *
     * @param url the url to hit
     * @return the HTTP response as a string or failure on error
     */
	protected String getResponse(String url) {
        WebClient webClient = new WebClient();
        webClient.setThrowExceptionOnFailingStatusCode(false);
        Page page = null;

        try {
        	page = webClient.getPage(url);
        } catch (MalformedURLException e) {
        	fail(e.toString());
        } catch (FailingHttpStatusCodeException e) {
            fail(e.toString());
        } catch (IOException e) {
        	fail(e.toString());
        }

        return page.getWebResponse().getContentAsString();
	}

    /**
     * Helper method for setting up mock responses for the tests.
     *
     * @param requestResponses an array of String request and responses to return
     * @param isOauth whether this service should use oauth
     * @param requestHeaders what expected additional headers each request to this service should have
     */
	protected void setupMockResponses(
            TestRequestResponse[] requestResponses,
            boolean isOauth,
            Map<String, String> requestHeaders) {
    	TestableFutureFactory futureFactory = new TestableFutureFactory();

		for (TestRequestResponse reqResp : requestResponses) {
	    	WS.HttpResponse mockHttpResponse = Mockito.mock(WS.HttpResponse.class);
	    	Mockito.when(mockHttpResponse.getString()).thenReturn(reqResp.getResponse());
	    	Mockito.when(mockHttpResponse.getStatus()).thenReturn(reqResp.getStatusCode());
			futureFactory.addHttpResponse(reqResp.getRequestUrl(), mockHttpResponse);
		}

        futureFactory.setExpectedRequestHeaders(requestHeaders);
        futureFactory.setOauthExpected(isOauth);

    	Application.futureFactory = futureFactory;
	}

    /**
     * Test the response from the API to see if the JSON looks as it
     * should, and if it's valid JSON.
     *
     * @param actualResponse the response returned to test
     * @param expectedRequestResponses a TestRequestResponse array of requests/responses to return for each future
     * 			(the array corresponds to the futures in alphabetical order). The number of futures
     * 			in the service should be the same as the length of this array.
     */
    protected void assertResponse(String actualResponse, TestRequestResponse[] expectedRequestResponses) {
        StringBuilder expectedString = new StringBuilder("{");
        String serviceIdentifier = getServiceBeingTested().getIdentifier();
        expectedString.append("\"" + serviceIdentifier + "\":{");


		for (int i = 0, n = expectedRequestResponses.length; i < n; i++) {
            String futureName = expectedRequestResponses[i].getName();
            String response = expectedRequestResponses[i].getResponse();

			if (i > 0) {
				expectedString.append(", ");
			}

			expectedString.append("\"");
			expectedString.append(futureName);
			expectedString.append("\":");

            // insert the mocked responses in order
            if (expectedRequestResponses[i].getStatusCode() != HttpStatus.SC_OK) {
                // If non-success status code, means the response should be a local error fragment.
                expectedString.append("{\"error\":");
                expectedString.append("{\"httpStatus\":").append(expectedRequestResponses[i].getStatusCode()).append(",");
                expectedString.append("\"errorMessage\":\"External service URL return non-success: ")
                        .append(serviceIdentifier).append(" ").append(futureName)
                        .append(" " + expectedRequestResponses[i].getRequestUrl()).append("\",");
                expectedString.append("\"responseData\":\"").append(response).append("\"}");
                expectedString.append("}");
            } else {

                // if need to wrap data, need to make sure to expect it
                if (expectedRequestResponses[i].getWrapWithData()) {
                    expectedString.append("{\"data\":").append(response).append("}");
                } else {
                    expectedString.append(response);
                }
            }
		}

        expectedString.append("}}");

    	assertEquals("Response should be as correct", expectedString.toString(), actualResponse);
    	assertTrue("Response should be valid JSON", isValidJSON(actualResponse));
    }

	/**
     * Tests if a string is valid json.
     *
     * @param stringToTest the string we are testing
     * @return a boolean which is true if json is valid
     */
	protected static Boolean isValidJSON(String stringToTest) {
		try {
			JSONObject.fromObject(stringToTest);
		} catch (JSONException e) {
			return false;
		}

		return true;
	}

	protected Map<String, ServiceUrlFuture> getServiceUrls(String serviceIdentifier, Params serviceParams) {
		return getServiceUrls(serviceIdentifier, serviceParams, Action.PROFILE);
	}
	
	/**
	 * Returns urls that are constructed for the desired service
	 *
	 * @param serviceIdentifier is the service requested
	 * @param serviceParams are the params for requested service
	 * @param action is type of action requested (profile, query, search, etc...)
	 * @return Map of future name to url
	 */
	protected Map<String, ServiceUrlFuture> getServiceUrls(String serviceIdentifier, Params serviceParams, Action action) {
		Service service = Application.SUPPORTED_SERVICES.get(serviceIdentifier.toLowerCase());
		Map<String, ServiceUrlFuture> urlsMap = new TreeMap<String, ServiceUrlFuture>(service.getUrls(action, serviceParams));

		return urlsMap;
	}

	/**
	 * Helper method that tests the http request url
	 *
	 * @param url is the http url that is being tested
	 * @param statusCode the expected http status code
	 * @param message the expected message
	 */
	protected void serviceGETCall(String url, int statusCode, String message) {
		Response response = GET(url);
		assertStatus(statusCode, response);
	    if (message != null) {
	    	assertContentMatch(message, response);
	    }
	}

    @Test
    public void testThatCallAllServicesRequiresParameters() {
    	serviceGETCall("/profile.json?services=li,tw,fb", 400, "Missing parameters for requested service: fb");
        serviceGETCall("/profile.json?services=li,tw,fb", 400, "Missing parameters for requested service: li");
        serviceGETCall("/profile.json?services=li,tw,fb", 400, "Missing parameters for requested service: tw");
    }

    @Test
    public void testThatCallRequiresValidService() {
    	serviceGETCall("/profile.json?services=newFad", 400, "Unknown service in services parameter: newFad");
    }

    @Test
    public void testThatCallDealsWithGlobalErrorsCorrectly() {
        // This test ensures global errors are present, and the correct successful JSON is returned for 1 service.
    	TestRequestResponse[] mockRequestResponses = {
                new TestRequestResponse(
                        "userInfo",
                        "https://graph.facebook.com/evanbeard?access_token=1234",
                        "{ \"fakeUserInfo\": 0 }"),
                new TestRequestResponse(
                        "userTimeline",
                        "https://graph.facebook.com/evanbeard/feed?access_token=1234",
                        "{ \"fakeUserTimeline\": 0 }",
                        true),
        };

        // Setup mock responses for Facebook.
        setupMockResponses(mockRequestResponses, false, new HashMap<String,String>());
        String actualResponse = getResponse("http://localhost:9000/profile.json?services=newFad,li,tw,fb&fbUserIdentifier=evanbeard&fbOauthToken=1234");

        // Assert JSON response is correct with globalErrors payload.
        StringBuilder expectedString = new StringBuilder("{");
        expectedString.append("\"globalErrors\":[");
        expectedString.append("\"Unknown service in services parameter: newFad\",");
        expectedString.append("\"Invalid or Missing parameters for requested service: li\",");
        expectedString.append("\"Invalid or Missing parameters for requested service: tw\"");
        expectedString.append("],");

        expectedString.append("\"" + "fb" + "\":{");

		for (int i = 0, n = mockRequestResponses.length; i < n; i++) {
            String futureName = mockRequestResponses[i].getName();
            String response = mockRequestResponses[i].getResponse();

			if (i > 0) {
				expectedString.append(", ");
			}

			expectedString.append("\"");
			expectedString.append(futureName);
			expectedString.append("\":");

            // insert the mocked responses in order
			expectedString.append(response);
		}

        expectedString.append("}}");

    	assertEquals("Response should be as correct", expectedString.toString(), actualResponse);
    	assertTrue("Response should be valid JSON", isValidJSON(actualResponse));

        teardownTestEnvironment();
    }

    @Test
    public void testThatSearchCallRequiresValidService() {
    	serviceGETCall("/search.json?services=newFad", 400, "Unknown service in services parameter: newFad");
    }

    @Test
    public void testThatSearchCallRequiresValidParams() {
    	serviceGETCall("/search.json?services=li", 400, "Missing parameters");
    }

    /**
     * Undoes global variable manipulation performed by the tests.
     * (For example, sets the FutureFactory back to the production
     * versions, so that after tests are run the server will still
     * emulate production for manual testing).
     */
    protected void teardownTestEnvironment() {
    	Application.futureFactory = new FutureFactory();
    }
}