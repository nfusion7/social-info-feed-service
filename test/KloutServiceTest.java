import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.mockito.Mockito;

import org.junit.*;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import play.Invoker.Suspend;
import play.test.*;
import play.libs.WS;
import play.mvc.*;
import play.mvc.Http.*;
import play.mvc.Scope.Params;

import services.*;
import testing.TestRequestResponse;

import java.util.HashMap;
import java.util.Map;

public class KloutServiceTest extends FeedServiceTest {
    private static final String MISSING_PARAMETERS = "Missing parameters for requested service: kl";
    private static final String KEY = "cx53fvwgmyst7zvz6svxnsq6";
    private static final String KEY_V2 = "4kd2zejawamr4fv4es7sv8nr";
    private static final String URL_SHOW = "http://api.klout.com/1/users/show.json?key="+KEY+"&users=";
    private static final String URL_TOPICS = "http://api.klout.com/1/users/topics.json?key="+KEY+"&users=";
    private static final String URL_INFLUENCED = "http://api.klout.com/1/soi/influenced_by.json?key="+KEY+"&users=";
    private static final String URL_INFLUENCER = "http://api.klout.com/1/soi/influencer_of.json?key="+KEY+"&users=";
    private static final String KLOUT_ID_PLACEHOOLDER = "$KLOUT_ID$";
    private static final String URL_SHOW_V2 = "http://api.klout.com/v2/user.json/"+KLOUT_ID_PLACEHOOLDER+"?key="+KEY_V2;
    private static final String URL_TOPICS_V2 = "http://api.klout.com/v2/user.json/"+KLOUT_ID_PLACEHOOLDER+"/topics?key="+KEY_V2;
    private static final String URL_INFLUENCE_V2 = "http://api.klout.com/v2/user.json/"+KLOUT_ID_PLACEHOOLDER+"/influence?key="+KEY_V2;
    private static final String IDENTITY_URL_V2 = "http://api.klout.com/v2/identity.json/tw?screenName=";

	/**
	 * Returns the service being tested.
	 *
	 * @return Service class being tested
	 */
	protected Service getServiceBeingTested() {
		return new KloutService();
	}

	@Test
	public void testThatCallKloutApiRequiresScreenName() {
		serviceGETCall("/profile.json?services=kl", 400, MISSING_PARAMETERS);
	}


	@Test
	public void testThatCallKloutApiUserIdentifierEmpty() {
		serviceGETCall("/profile.json?services=kl&klUserIdentifier=", 400,
		MISSING_PARAMETERS);
	}


	@Test
	public void testThatCallKloutApiParamsInCaps() {
		serviceGETCall(
				"/profile.json?SERVICES=KL&KLUSERIDENTIFIER=evanbeard",
				400, "Invalid services parameter string");
	}

	@Test
	public void testThatCallKloutApiReturnsJSONCorrectlyIfThereAreNoErrors_v1() {
		TestRequestResponse[] mockRequestResponses = {
				new TestRequestResponse(
						"userInfluencedBy",
						URL_INFLUENCED + "mark",
						"{ \"fakeUserInfluencedBy\": 0 }"),
				new TestRequestResponse(
						"userInfluencerOf",
						URL_INFLUENCER + "mark",
						"{ \"fakeUserInfluenceOf\": 0 }"),
				new TestRequestResponse(
						"userShow",
						URL_SHOW + "mark",
						"{ \"fakeUseShow\": 0 }"),
				new TestRequestResponse(
						"userTopics",
						URL_TOPICS + "mark",
						"{ \"fakeUserTopics\": 0 }"),
		};

		setupMockResponsesForKlout(mockRequestResponses);

		String serviceResponse = getResponse("http://localhost:9000/profile.json?services=kl&klUserIdentifier=mark");
		assertResponse(serviceResponse, mockRequestResponses);
		teardownTestEnvironment();
	}

    @Test
    public void testThatCallKloutApiDealsWithLocalErrorsCorrectly_v1() {
        // This test ensures local errors are present, and the correct successful JSON is returned for 1 service.
    	TestRequestResponse[] mockRequestResponses = {
				new TestRequestResponse(
						"userInfluencedBy",
						URL_INFLUENCED + "mark",
						"fake error string - influenced",
						500),
				new TestRequestResponse(
						"userInfluencerOf",
						URL_INFLUENCER + "mark",
						"fake error string - influencer",
						400),
				new TestRequestResponse(
						"userShow",
						URL_SHOW + "mark",
						"fake error string - show",
						400),
				new TestRequestResponse(
						"userTopics",
						URL_TOPICS + "mark",
						"fake error string - topic",
						500),
        };

        setupMockResponsesForKlout(mockRequestResponses);

        String serviceResponse = getResponse("http://localhost:9000/profile.json?services=kl&klUserIdentifier=mark");
        assertResponse(serviceResponse, mockRequestResponses);
        teardownTestEnvironment();
    }

	@Test
	public void testKloutServiceGetUrlsIsValid_v1() {
		Params serviceParams = new Params();

		serviceParams.put("twUserIdentifier", "tweeter"); // should not be included in Klout request
		serviceParams.put("klUserIdentifier", "duplicateParam"); // should be ignored because is overwritten later
		serviceParams.put("klUserIdentifier", "pirate");
		Map<String, ServiceUrlFuture> actualUrls = getServiceUrls("kl", serviceParams);

		String expectedUserShowRequest = URL_SHOW + "pirate";
		String expectedUserTopicsRequest = URL_TOPICS + "pirate";
		String expectedUserInfluencedRequest = URL_INFLUENCED + "pirate";
		String expectedUserInfluencerRequest = URL_INFLUENCER + "pirate";

		assertEquals("UserInfo request for Klout incorrect", expectedUserShowRequest, actualUrls.get("userShow").getUrl());
        assertEquals("UserInfo request for Klout incorrect", false, actualUrls.get("userShow").getWrapWithData());
        assertEquals("UserInfo request for Klout incorrect", expectedUserTopicsRequest, actualUrls.get("userTopics").getUrl());
        assertEquals("UserInfo request for Klout incorrect", false, actualUrls.get("userTopics").getWrapWithData());
        assertEquals("UserInfo request for Klout incorrect", expectedUserInfluencedRequest, actualUrls.get("userInfluencedBy").getUrl());
        assertEquals("UserInfo request for Klout incorrect", false, actualUrls.get("userInfluencedBy").getWrapWithData());
        assertEquals("UserInfo request for Klout incorrect", expectedUserInfluencerRequest, actualUrls.get("userInfluencerOf").getUrl());
        assertEquals("UserInfo request for Klout incorrect", false, actualUrls.get("userInfluencerOf").getWrapWithData());
	}
	
	@Test
	public void testThatCallKloutApiReturnsJSONCorrectlyIfThereAreNoErrors_v2() {
		TestRequestResponse[] mockRequestResponses = {
				new TestRequestResponse(
						"userInfluence",
						URL_INFLUENCE_V2.replace(KLOUT_ID_PLACEHOOLDER, "123456"),
						"{ \"fakeUserInfluence\": 0 }"),
				new TestRequestResponse(
						"userShow",
						URL_SHOW_V2.replace(KLOUT_ID_PLACEHOOLDER, "123456"),
						"{ \"fakeUseShow\": 0 }"),
				new TestRequestResponse(
						"userTopics",
						URL_TOPICS_V2.replace(KLOUT_ID_PLACEHOOLDER, "123456"),
						"{ \"fakeUserTopics\": 0 }"),
		};

		setupMockResponsesForKlout(mockRequestResponses);

		String serviceResponse = getResponse("http://localhost:9000/profile.json?services=kl&klUserIdentifier=123456&klApiVersion=2");
		assertResponse(serviceResponse, mockRequestResponses);
		teardownTestEnvironment();
	}
	
    @Test
    public void testThatCallKloutApiDealsWithLocalErrorsCorrectly_v2() {
        // This test ensures local errors are present, and the correct successful JSON is returned for 1 service.
    	TestRequestResponse[] mockRequestResponses = {
				new TestRequestResponse(
						"userInfluence",
						URL_INFLUENCE_V2.replace(KLOUT_ID_PLACEHOOLDER, "123456"),
						"fake error string - influence",
						400),
				new TestRequestResponse(
						"userShow",
						URL_SHOW_V2.replace(KLOUT_ID_PLACEHOOLDER, "123456"),
						"fake error string - show",
						400),
				new TestRequestResponse(
						"userTopics",
						URL_TOPICS_V2.replace(KLOUT_ID_PLACEHOOLDER, "123456"),
						"fake error string - topic",
						500),
        };

        setupMockResponsesForKlout(mockRequestResponses);

        String serviceResponse = getResponse("http://localhost:9000/profile.json?services=kl&klUserIdentifier=123456&klApiVersion=2");
        assertResponse(serviceResponse, mockRequestResponses);
        teardownTestEnvironment();
    }
    
	@Test
	public void testKloutServiceGetUrlsIsValid_v2() {
		Params serviceParams = new Params();

		serviceParams.put("twUserIdentifier", "tweeter"); // should not be included in Klout request
		serviceParams.put("klUserIdentifier", "duplicateParam"); // should be ignored because is overwritten later
		serviceParams.put("klUserIdentifier", "666666");
		serviceParams.put("klApiVersion", "2");
		Map<String, ServiceUrlFuture> actualUrls = getServiceUrls("kl", serviceParams);

		String expectedUserShowRequest = URL_SHOW_V2.replace(KLOUT_ID_PLACEHOOLDER, "666666");
		String expectedUserTopicsRequest = URL_TOPICS_V2.replace(KLOUT_ID_PLACEHOOLDER, "666666");
		String expectedUserInfluenceRequest = URL_INFLUENCE_V2.replace(KLOUT_ID_PLACEHOOLDER, "666666");

		assertEquals("UserInfo request for Klout incorrect", expectedUserShowRequest, actualUrls.get("userShow").getUrl());
        assertEquals("UserInfo request for Klout incorrect", false, actualUrls.get("userShow").getWrapWithData());
        assertEquals("UserInfo request for Klout incorrect", expectedUserTopicsRequest, actualUrls.get("userTopics").getUrl());
        assertEquals("UserInfo request for Klout incorrect", false, actualUrls.get("userTopics").getWrapWithData());
        assertEquals("UserInfo request for Klout incorrect", expectedUserInfluenceRequest, actualUrls.get("userInfluence").getUrl());
        assertEquals("UserInfo request for Klout incorrect", false, actualUrls.get("userInfluence").getWrapWithData());
	}
	
	@Test
	public void testKloutQuery() {
		TestRequestResponse[] mockRequestResponses = {
				new TestRequestResponse(
						"apiQueryResult",
						IDENTITY_URL_V2+"SteveMartin",
						"{ \"fakeUserQuery\": 0 }")
		};

		setupMockResponsesForKlout(mockRequestResponses);

		String serviceResponse = getResponse("http://localhost:9000/query.json?services=kl&apiQueryUrl="+IDENTITY_URL_V2+"SteveMartin");
		assertResponse(serviceResponse, mockRequestResponses);
		teardownTestEnvironment();
	}
	
	@Test
	public void testKloutQueryRequiresQueryUrl() {
		serviceGETCall("/query.json?services=kl", 400,
		"Missing parameters");
	}
	
	@Test
	public void testKloutQueryEmptyQueryString() {
		serviceGETCall("/query.json?services=kl&apiQueryUrl=", 400,
		"Missing parameters");
	}
	
	@Test
	public void testKloutQueryInvalidEndpoint() {
		serviceGETCall("/query.json?services=kl&apiQueryUrl=http://ap.klout.com/somequeryurl", 400,
		"Missing parameters");
	}
	
	private void setupMockResponsesForKlout(TestRequestResponse[] mockRequestResponses) {
		setupMockResponses(mockRequestResponses, false,	new HashMap<String, String>());
	}
}